function elide(id) {
    document.getElementById("elidelink"+id).style.display="none";
    document.getElementById("elide"+id).style.display="block";
}

function makeElide(id) {
    document.write('<a href="javascript:elide('+id+')">...</a>');
}

function expandSpoiler(tag) {
    for(i in tag.parentNode.childNodes) {
        if(tag.parentNode.childNodes[i].nodeName=='DIV') {
            if(tag.parentNode.childNodes[i].style.display=='block')
                tag.parentNode.childNodes[i].style.display='none';
            else
                tag.parentNode.childNodes[i].style.display='block';
        }
    }
}

function addPollOption() {
    var val = document.getElementById("pollCount").value*1 + 1;
    document.getElementById("pollCount").value = val;
    var opt = document.getElementById("pollOptions");
    var str = document.getElementById("pollTemplate").innerHTML;
    str = str.replace(/{ID}/g, val);
    opt.innerHTML += str;
}

function deletePollOption(id) {
    document.getElementById("poll"+id).outerHTML='';
}
