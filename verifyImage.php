<?php
header("Content-type: image/png");
include_once("include/config.php");

$fonts = array('arial','verdana','times','courb');
$str = verifyString($_REQUEST['key']);

$img = imagecreatetruecolor(250,50);

$bg = imagecolorallocate($img, 255, 255, 255);

imagefill($img, 0, 0, $bg);

for($i=0;$i<strlen($str);$i++) {
    $fg = imagecolorallocate($img, rand(0,64), rand(0,64), rand(0,64));
    imagettftext($img, rand(20,30), rand(-15,60), 20+$i*40, 40, $fg, $fonts[rand(0,3)], $str[$i]); 
}

for($i=0;$i<2000;$i++) {
    $static = imagecolorallocate($img, rand(30,200), rand(30,200), rand(30,200));
    $x = rand(0,250); $y = rand(0,50);
    imageline($img, $x, $y, $x+rand(-1,1), $y+rand(-2,2), $static);
}

imagepng($img);

?>
