<?php
include_once('include/config.php');

if(is_numeric($_REQUEST['page'])) {
    $start = ($_REQUEST['page']-1) * $config['setup']['threadsPerPage']; 
    $page = $_REQUEST['page'];
} else {
    $start = 0;
    $page = 1;
}

$screen = newPage($errors['local']['members']);
$table = new Template("member.body.html");
if(isset($_REQUEST['order'])) {
    $table->assign("DESCUSER", empty($_REQUEST['desc'])&&($_REQUEST['order']=='username')?'&desc=1':'');
    $table->assign("DESCRANK", empty($_REQUEST['desc'])&&($_REQUEST['order']=='rank')?'&desc=1':'');
    $table->assign("DESCDATE", empty($_REQUEST['desc'])&&($_REQUEST['order']=='registerDate')?'&desc=1':'');
    $table->assign("DESCPC", empty($_REQUEST['desc'])&&($_REQUEST['order']=='pc')?'&desc=1':'');
} else {
    $table->assign("DESCUSER", '');
    $table->assign("DESCRANK", '');
    $table->assign("DESCDATE", '&desc=1');
    $table->assign("DESCPC", '');
}

if($config['user']['rank'] < 2)
    $where = "WHERE regConfirmed='1' AND adminConfirmed='1' AND coppaConfirmed='1'";
else
    $where = '';

$ucount = $db->execute("SELECT COUNT(*) FROM {$prefix}_users $where")->fetchField();
$table->condition("PAGER", $ucount > $config['setup']['threadsPerPage']);
$table->assign("PAGER", makePager('member.php'.(isset($_REQUEST['order'])?'?order='.$_REQUEST['order'].(isset($_REQUEST['desc'])?'&desc=1':''):''),
    $ucount, $config['setup']['threadsPerPage'], $page, true));

$ranks = array(-1=>array(), 0=>array(), 1=>array(), 2=>array(), 3=>array(), 4=>array());
foreach($db->execute("SELECT rank, minPosts, image, title FROM {$prefix}_ranks")->iterator() as $row) {
    $ranks[$row['rank']][$row['minPosts']] = array("image"=>$row['image'], "title"=>$row['title']);
}

$query = "SELECT userID, username, rank, title, customRank, location, COUNT(DISTINCT postID) pc, registerDate ";
$query .= "FROM {$prefix}_users u LEFT JOIN {$prefix}_posts p ON u.userID=p.creatorID ";
$query .= "$where GROUP BY u.userID ";
if(isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('username','rank','registerDate','pc'))) {
    $query .= "ORDER BY $_REQUEST[order] ";
    if(isset($_REQUEST['desc'])) $query .= "DESC ";
}
$query .= "LIMIT $start, ".$config['setup']['threadsPerPage'];

$rows = '';
foreach($db->execute($query)->iterator() as $row) {
    $line = new Template("member.line.html");
    $line->assign("USERID", $row['userID']);
    $line->assign("NAME", str_replace(" ","&nbsp;",$row['username']));
    $line->assign("POSTS", $row['pc']);
    $line->assign("LOCATION", $row['location']);
    $line->assign("JOINED", formatTime($row['registerDate']));
    if($row['title']=='' || $row['customRank']=='') {
        if(!isset($ranks[$row['rank']][$row['pc']])) {
            $t = 0;
            foreach($ranks[$row['rank']] as $pc => $rank) {
                if($row['pc']<$pc) break;
                $t = $pc;
            }
            $ranks[$row['rank']][$row['pc']] = $ranks[$row['rank']][$t];
        }
    }
    $line->assign("TITLE", str_replace(' ','&nbsp;',$row['title']==''?$ranks[$row['rank']][$row['pc']]['title']:$row['title']));
    $line->assign("RANK", imageLink($row['customRank']==''?$ranks[$row['rank']][$row['pc']]['image']:$row['customRank']), $errors['rank'][$row['rank']]);
    $rows .= $line->html();
}

$table->assign("ROWS", $rows);
$screen->assign("BODY", $table->html());
echo $screen->html();

?>
