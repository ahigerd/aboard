<?php
include_once('include/config.php');

$title = $errors['local']['log_in'];
if(isset($_REQUEST['action']) && $_REQUEST['action']=='logout') {
    clearCookie("userID");
    clearCookie("hash");
    $userID=0;
    $config['user']['userID']=0;
    $config['user']['rank']=0;
    $body = new Template("login.out.html");
    $title = $errors['local']['log_out'];
} elseif(isset($_REQUEST['action'])) { // log in
    $hash = md5($_REQUEST['password']);
    $user = $db->execute("SELECT userID, regConfirmed, adminConfirmed, coppaConfirmed ".
        "FROM {$prefix}_users WHERE username='$_REQUEST[username]' AND password='".md5($_REQUEST['password'])."'")->fetchAssoc();
    $good = $user && $user['regConfirmed'] && $user['adminConfirmed'] && $user['coppaConfirmed'];
    if($good) {
        storeCookie("userID", $user['userID']);
        storeCookie("hash", $hash);
        $userID = validateLogin();
        $body = new Template("login.in.html");
    } else {
        $body = new Template("login.form.html");
        $body->condition("ERROR", true);
        $body->condition("RESEND", $user && !$user['regConfirmed']);
        if(!$user)
            $body->assign("ERROR", $errors['login']['no_match'].'<br/>');
        else
            $body->assign("ERROR", $errors['login']['not_active'].'<br/>');
    }
} else {
    $body = new Template("login.form.html");
    $body->condition("ERROR", false);
    $body->assign("ERROR", '');
    $body->condition("RESEND", false);
}

$screen = newPage($title);
$screen->assign("BODY", $body->html());
echo $screen->html();
?>
