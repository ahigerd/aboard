<?php
include_once('include/config.php');

if(!isset($_REQUEST['id']) || !is_numeric($_REQUEST['id'])) {
    header("Location: index.php");
    exit;
}

$user = $db->execute("SELECT activationKey, adminConfirmed, coppaConfirmed FROM {$prefix}_users WHERE userID='$_REQUEST[id]'")->fetchAssoc();
if(!$user || !isset($_REQUEST['key']) || $_REQUEST['key']!=$user['activationKey']) {
    header("Location: index.php");
    exit;
}

$db->execute("UPDATE {$prefix}_users SET regConfirmed='1' WHERE userID='$_REQUEST[id]'");

$screen = newPage($error['local']['account_activation']);
$body = new Template("register.activated.html");
$body->assign("ADMIN", !($user['adminConfirmed']||$user['coppaConfirmed']));
$screen->assign("BODY", $body->html());
echo $screen->html();

?>
