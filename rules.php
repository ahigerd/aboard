<?php
include_once('include/config.php');

if(isset($_POST['confirm']) && $userID) {
    $db->execute("UPDATE {$prefix}_users SET rulesConfirmed='1' WHERE userID='$userID'");
    header("Location: index.php");
    exit;
}

$screen = newPage($errors['local']['rules']);
$body = new Template("rules.body.html");

$rows = '';
$i = 0;
foreach($db->execute("SELECT * FROM {$prefix}_rules ORDER BY position ASC")->iterator() as $row) {
    $i++;
    $line = new Template("rules.line.html");
    $line->assign("NUMBER", $i);
    $line->assign("RULEID", $row['ruleID']);
    $line->assign("TEXT", formatText($row['body']));
    $rows .= $line->html();
}

$body->assign("ROWS", $rows);
$body->condition("ERROR", isset($_POST['button']) && !isset($_POST['confirm']));
$body->condition("CONFIRM", $userID && !$config['user']['rulesConfirmed']);
$screen->assign("BODY", $body->html());
echo $screen->html();
?>
