<?php
include_once("include/config.php");

/**
 * Checks the logged-in user's permission to edit the thread
 * @param int thread ID, or false to check by post ID
 * @param int postID, or false to check by thread ID
 * @return boolean permission granted
 */
function canModThread($threadID, $postID = false) {
    global $db, $userID, $config, $prefix;
    if(!$threadID && !$postID) return false;
    if($threadID && !ctype_digit($threadID)) return false;
    if($postID && !ctype_digit($postID)) return false;
    $query = "SELECT minimumMod, ";
    if($userID)
        $query .= "CASE WHEN COUNT(k.userID) > 0 THEN -1 ELSE IFNULL(MAX(ga.actingRank), a.actingRank) END AS actingRank, IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank ";
    else
        $query .= "NULL AS actingRank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END AS rank ";
    $query .= "FROM {$prefix}_threads t INNER JOIN {$prefix}_forums f ON t.forumID=f.forumID ";
    if($postID) $query .= "INNER JOIN {$prefix}_posts p ON t.threadID=p.threadID ";
    $query .= "LEFT JOIN {$prefix}_kicks k ON (t.threadID=k.threadID AND k.userID='$userID') ";
    $query .= "LEFT JOIN {$prefix}_groups g ON f.groupID=g.groupID ";
    $query .= "LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$userID}' ";
    $query .= "LEFT JOIN {$prefix}_group_members gm ON gm.userID='{$userID}' ";
    $query .= "LEFT JOIN {$prefix}_group_access ga ON gm.groupID=ga.groupID AND ga.forumID=f.forumID ";
    $query .= "LEFT JOIN {$prefix}_access a ON (a.forumID=f.forumID AND a.userID='$userID') ";
    if($postID)
        $query .= "WHERE p.postID='$postID'";
    else
        $query .= "WHERE t.threadID='$threadID'";
    $row = $db->execute($query)->fetchAssoc();
    if($row['actingRank'] == -1) return false;
    return compareRank($row, $row['minimumMod'], 2);
}

$threadID='';

try {
    $screen = false;
    if(isset($_REQUEST['edit'])) {
        setGroupByPostID($_REQUEST['edit']);
        $screen = newPage($errors['local']['editing']);
        if(!$userID || !is_numeric($_REQUEST['edit'])) throw new Exception("denied.html");
        $query = "SELECT p.*, t.threadID, f.forumID, minimumPost, minimumMod, isLocked, k.threadID AS kick, f.name AS forumName, t.title, ";
        if($userID)
            $query .= "CASE WHEN COUNT(k.userID) > 0 THEN -1 ELSE IFNULL(ga.actingRank, a.actingRank) END AS actingRank, IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank ";
        else
            $query .= "NULL AS actingRank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END AS rank ";
        $query .= "FROM {$prefix}_posts p INNER JOIN {$prefix}_threads t ON p.threadID=t.threadID ";
        $query .= "INNER JOIN {$prefix}_forums f ON t.forumID=f.forumID ";
        $query .= "LEFT JOIN {$prefix}_groups g ON f.groupID=g.groupID ";
        $query .= "LEFT JOIN {$prefix}_kicks k ON (t.threadID=k.threadID AND k.userID='$userID') ";
        $query .= "LEFT JOIN {$prefix}_access a ON (a.forumID=f.forumID AND a.userID='$userID') ";
        $query .= "LEFT JOIN {$prefix}_group_members m ON m.userID='$userID' ";
        $query .= "LEFT JOIN {$prefix}_group_access ga ON g.groupID=ga.groupID AND ga.forumID=f.forumID ";
        $query .= "WHERE p.postID='$_REQUEST[edit]'";
        $post = $db->execute($query)->fetchAssoc();
        $threadID=$post['threadID'];
        $targetRank = ($post['creatorID'] == $userID) ? 1 : $post['minimumMod'];
        $restrict = $config['user']['denyEdit'] || $post['kick'] || !compareRank($post, $targetRank);
        if($restrict) throw new Exception("denied.html");

        $firstPost = !$db->execute("SELECT COUNT(*) FROM {$prefix}_posts WHERE postID<'$_REQUEST[edit]' AND threadID='$post[threadID]'")->fetchField();
        
        if(isset($_REQUEST['post']) && !($firstPost && empty($_REQUEST['subject'])) && !empty($_REQUEST['body'])) {
            $_REQUEST['body'] .= "\n\n[i]Edited by ".$config['user']['username']."[/i]";
            if(!$db->execute("SELECT COUNT(*) FROM {$prefix}_posts p INNER JOIN {$prefix}_views v ON p.threadID=v.threadID ".
                             "WHERE userID='$userID' AND p.timestamp>v.lastView")->fetchField())
                $db->execute("REPLACE INTO {$prefix}_views SET userID='$userID', threadID='$threadID', lastView=NOW()");
            $query = "UPDATE {$prefix}_posts SET body='".addslashes($_REQUEST['body'])."', ".($userID==$post['creatorID']?"timestamp='$_NOW', ":'');
            $query .= "disableSig='".(isset($_REQUEST['disableSig'])?1:0)."', disableTags='".(isset($_REQUEST['disableTags'])?1:0)."', ";
            $query .= "disableSmilies='".(isset($_REQUEST['disableSmilies'])?1:0)."' WHERE postID='$_REQUEST[edit]'";
            $db->execute($query);
            if($firstPost) $db->execute("UPDATE {$prefix}_threads SET title='".addslashes($_REQUEST['subject'])."' WHERE threadID='$threadID'");
            throw new Exception("mod.editsuccess.html");
        }
        
        $page = new Template("mod.edit.html");
        $error = "";
        if(isset($_REQUEST['post']) || isset($_REQUEST['preview'])) {
            if($firstPost && empty($_REQUEST['subject'])) $error .= $errors['post']['no_subject'] . '<br/>';
            if(empty($_REQUEST['body'])) $error .= $errors['post']['no_body'].'<br/>';
        }
        $page->assign("FORUMNAME", $post['forumName']);
        $page->assign("FORUMID", $post['forumID']);
        $page->assign("THREADNAME", htmlspecialchars($post['title'], ENT_QUOTES));
        $page->assign("THREADID", $post['threadID']);
        $page->assign("POSTID", $_REQUEST['edit']);
        if(!isset($_REQUEST['subject'])) $_REQUEST['subject']=$post['title'];
        $page->assign("SUBJECT", htmlspecialchars($_REQUEST['subject'], ENT_QUOTES));
        $page->condition("THREAD", $firstPost);
        
        if(!isset($_REQUEST['body'])) $_REQUEST['body'] = $post['body'];
        $page->condition("PREVIEW", isset($_REQUEST['preview']));
        $page->assign("PREVIEW", (isset($_REQUEST['preview'])?formatText($_REQUEST['body'], $userID, FORMAT_PREVIEW):''));
        if(isset($_REQUEST['preview']) && $spellError) $error .= $errors['post']['spelling']; 

        $page->condition("ERROR", $error);
        $page->assign("ERROR", $error);
        $page->assign("BODY", htmlspecialchars($_REQUEST['body'], ENT_QUOTES));

        if($post['disableSig']) $_REQUEST['disableSig']=1;
        if($post['disableTags']) $_REQUEST['disableTags']=1;
        if($post['disableSmilies']) $_REQUEST['disableSmilies']=1;
        
        $page->assign("DISABLESIG", isset($_REQUEST['disableSig'])?'checked="true"':'');
        $page->assign("DISABLETAGS", isset($_REQUEST['disableTags'])?'checked="true"':'');
        $page->assign("DISABLESMILIES", isset($_REQUEST['disableSmilies'])?'checked="true"':'');
        
        $screen->assign("BODY", $page->html());
        echo $screen->html();
        exit;
    }
    $threadID = false;
    if(isset($_REQUEST['delete'])) {
        setGroupByPostID($_REQUEST['delete']);
    } else {
        foreach(array('lock', 'sticky', 'announce', 'delthread', 'kick', 'unkick') as $key) {
            if(isset($_REQUEST[$key])) {
                $threadID = $_REQUEST[$key];
                break;
            }
            setGroupByThreadID($threadID);
        }
    }
    $screen = newPage($errors['local']['mod_cp']);
    if(!$userID) throw new Exception("denied.html");
    if(isset($_REQUEST['lock'])) {
        if(!canModThread($_REQUEST['lock'])) throw new Exception("denied.html");
        $db->execute("UPDATE {$prefix}_threads SET isLocked=CASE WHEN isLocked THEN 0 ELSE 1 END WHERE threadID='$_REQUEST[lock]'");
        $page = new Template("mod.threadsuccess.html");
        $page->assign("THREADID", $_REQUEST['lock']);
    } elseif(isset($_REQUEST['sticky'])) {
        if(!canModThread($_REQUEST['sticky'])) throw new Exception("denied.html");
        $db->execute("UPDATE {$prefix}_threads SET isSticky=CASE WHEN isSticky THEN 0 ELSE 1 END WHERE threadID='$_REQUEST[sticky]'");
        $page = new Template("mod.threadsuccess.html");
        $page->assign("THREADID", $_REQUEST['sticky']);
    } elseif(isset($_REQUEST['announce'])) {
        if(!canModThread($_REQUEST['announce']) || $config['user']['rank']<3) throw new Exception("denied.html");
        $db->execute("UPDATE {$prefix}_threads SET isSticky='2' WHERE threadID='$_REQUEST[announce]'");
        $page = new Template("mod.threadsuccess.html");
        $page->assign("THREADID", $_REQUEST['announce']);
    } elseif(isset($_REQUEST['delthread'])) {
        if(!canModThread($_REQUEST['delthread'])) throw new Exception("denied.html");
        if(!isset($_REQUEST['confirm'])) {
            $page = new Template("mod.confirmdelthread.html");
            $page->assign("SUBJECT", $db->execute("SELECT title FROM {$prefix}_threads WHERE threadID='$_REQUEST[delthread]'")->fetchField());
            $page->assign("THREADID", $_REQUEST['delthread']);
        } else {
            $page = new Template("mod.forumsuccess.html");
            $page->assign("FORUMID", $db->execute("SELECT forumID FROM {$prefix}_threads WHERE threadID='$_REQUEST[delthread]'")->fetchField());
            $db->execute("DELETE FROM {$prefix}_threads WHERE threadID='$_REQUEST[delthread]'");
            $db->execute("DELETE FROM {$prefix}_posts WHERE threadID='$_REQUEST[delthread]'");
        }
    } elseif(isset($_REQUEST['delete'])) {
        if(!canModThread(false, $_REQUEST['delete'])) throw new Exception("denied.html");
        if(!isset($_REQUEST['confirm'])) {
            $page = new Template("mod.confirmdelete.html");
            $page->assign("POSTID", $_REQUEST['delete']);
        } else {
            $threadID = $db->execute("SELECT threadID FROM {$prefix}_posts WHERE postID='$_REQUEST[delete]'")->fetchField();
            $db->execute("DELETE FROM {$prefix}_posts WHERE postID='$_REQUEST[delete]'");
            if($db->execute("SELECT COUNT(*) FROM {$prefix}_posts WHERE threadID='$threadID'")->fetchField() == 0) {
                $db->execute("DELETE FROM {$prefix}_threads WHERE threadID='$threadID'");
                throw new Exception("mod.forumsuccess.html");
            } else {
                throw new Exception("mod.threadsuccess.html");
            }
        }
    } elseif(isset($_REQUEST['kick'])) {
        if(!canModThread($_REQUEST['kick'])) throw new Exception("denied.html");
        $page = new Template("mod.kicks.body.html");
        $error = false;
        if(isset($_REQUEST['kickByName']) && !empty($_REQUEST['kickName'])) {
            $query = "SELECT userID FROM {$prefix}_users WHERE username='".addslashes($_REQUEST['kickName'])."'";
            $kickID = $db->execute($query)->fetchField();
            if(!$kickID) {
                $page->assign("ERROR", $errors['mod']['no_such_user']);
                $error = true;
            } else {
                $_REQUEST['id'] = "$kickID"; // wrapped in a string to sidestep a bug in some versions of PHP
            }
        }
        if(isset($_REQUEST['id']) && ctype_digit($_REQUEST['id'])) {
            if($_REQUEST['id'] == $userID) {
                $page->assign("ERROR", $errors['mod']['no_kick_self']);
                $error = true;
            } else {
                $isKicked = $db->execute("SELECT COUNT(*) FROM {$prefix}_kicks WHERE threadID={$_REQUEST['kick']} AND userID={$_REQUEST['id']}")->fetchField();
                if($isKicked) {
                    $page->assign("ERROR", $errors['mod']['already_kicked']);
                    $error = true;
                } else {
                    $db->execute("INSERT INTO {$prefix}_kicks (threadID, userID) VALUES ({$_REQUEST['kick']}, {$_REQUEST['id']})");
                    header("Location: mod.php?kick={$_REQUEST['kick']}");
                    exit;
                }
            }
        }
        if(isset($_REQUEST['unkick'])) {
            if(!empty($_REQUEST['unkickID'])) {
                $clause = implode(',', array_keys($_REQUEST['unkickID']));
                if(preg_match("/^[0-9,]$/", $clause)) {
                    $db->execute("DELETE FROM {$prefix}_kicks WHERE threadID='{$_REQUEST['kick']}' AND userID IN ($clause)");
                    header("Location: mod.php?kick={$_REQUEST['kick']}");
                    exit;
                }
            }
        }
        $page->condition("ERROR", $error);
        $page->assign("THREADID", $_REQUEST['kick']);
        $query = "SELECT f.forumID, f.name AS forumName, t.title
                  FROM {$prefix}_threads t INNER JOIN {$prefix}_forums f ON t.forumID=f.forumID
                  WHERE t.threadID='{$_REQUEST['kick']}'";
        $row = $db->execute($query)->fetchAssoc();
        $page->assign("FORUMID", $row['forumID']);
        $page->assign("FORUMNAME", $row['forumName']);
        $page->assign("THREADNAME", $row['title']);
        $query = "SELECT IFNULL(u.username, 'Deleted User') AS username, k.userID 
                  FROM {$prefix}_kicks k LEFT JOIN {$prefix}_users u ON k.userID=u.userID
                  WHERE k.threadID='{$_REQUEST['kick']}' ORDER BY username";
        $lines = '';
        $even = true;
        foreach($db->execute($query)->iterator() as $row) {
            $even = !$even;
            $line = new Template("mod.kicks.line.html");
            $line->assign("USERID", $row['userID']);
            $line->assign("USERNAME", $row['username']);
            $line->condition("EVEN", $even);
            $lines .= $line->html();
        }
        $page->assign("ROWS", $lines);
        $page->condition("HASKICKS", $lines != '');
    } else {
        throw new Exception("denied.html");
    }
} catch(Exception $e) {
    $page = new Template($e->getMessage());
    $page->assign("THREADID", $threadID);
}
if(!$screen) {
    header("Location: index.php");
    exit;
}
$screen->assign("BODY", $page->html());
echo $screen->html();
?>
