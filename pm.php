<?php
include_once('include/config.php');
if(empty($userID)) {
    header("Location: index.php");
    exit;
}

$selected = array();
if(isset($_REQUEST['selected'])) {
    foreach($_REQUEST['selected'] as $id) {
        if(ctype_digit("$id"))
            $selected[] = $id;
    }
    $selectedClause = implode(',', $selected);
}

if(isset($_POST['delete'])) {
    if(count($selected)) {
        $db->execute("UPDATE {$prefix}_privmsg SET fromDeleted='1' WHERE fromDeleted='0' AND fromID='$userID' AND msgID IN ($selectedClause)"); 
        $db->execute("UPDATE {$prefix}_privmsg SET toDeleted='1' WHERE toDeleted='0' AND toID='$userID' AND msgID IN ($selectedClause)"); 
        $db->execute("DELETE FROM {$prefix}_privmsg WHERE fromDeleted='1' AND toDeleted='1'");
    }
    header("Location: pm.php?box={$_REQUEST['box']}");
    exit;
}

if(isset($_POST['move'])) {
    if(count($selected) && ctype_digit($_REQUEST['moveTo'])) {
        $db->execute("UPDATE {$prefix}_privmsg SET boxID='{$_REQUEST['moveTo']}' WHERE toID='$userID' AND msgID IN ($selectedClause)"); 
    }
    header("Location: pm.php?box={$_REQUEST['moveTo']}");
}

if(isset($_REQUEST['read'])) {
    if(ctype_digit($_REQUEST['read'])) {
        $query = "SELECT p.*, CASE WHEN f.userID IS NULL THEN p.fromName ELSE f.username END AS fromName,
                         CASE WHEN t.userID IS NULL THEN p.toName ELSE t.username END AS toName,
                         r.subject AS replySubject
                  FROM {$prefix}_privmsg p
                  LEFT JOIN {$prefix}_users f ON p.fromID=f.userID
                  LEFT JOIN {$prefix}_users t ON p.toID=t.userID
                  LEFT JOIN {$prefix}_privmsg r ON p.replyID=r.msgID AND ((r.fromID='$userID' AND r.fromDeleted='0') OR (r.toID='$userID' AND r.toDeleted='0'))
                  WHERE p.msgID='{$_REQUEST['read']}' AND ((p.fromID='$userID' AND p.fromDeleted='0') OR (p.toID='$userID' AND p.toDeleted='0'))";
        $pm = $db->execute($query)->fetchAssoc();
    } else {
        $pm = false;
    }
    if(!$pm) {
        header("Location: pm.php");
        exit;
    }
    if($pm['isRead'] == 0 && $pm['toID'] == $userID)
        $db->execute("UPDATE {$prefix}_privmsg SET isRead='1' WHERE msgID='{$pm['msgID']}'");
    $screen = newPage($errors['local']['pm']);
    $page = new Template("pm.read.html");
    $page->assign("MSGID", $pm['msgID']);
    $page->condition("FROM", $pm['fromID']);
    $page->assign("FROM", $pm['fromName']);
    $page->assign("FROMID", $pm['fromID']);
    $page->condition("TO", $pm['toID']);
    $page->assign("TO", $pm['toName']);
    $page->assign("TOID", $pm['toID']);
    $page->assign("SUBJECT", $pm['subject']);
    $page->condition("REPLY", $pm['replyID']);
    $page->assign("REPLY", $pm['replySubject']);
    $page->assign("REPLYID", $pm['replyID']);
    $format = ($pm['disableTags'] ? FORMAT_NO_TAGS : 0) | ($pm['disableSmilies'] ? FORMAT_NO_SMILIES : 0);
    $page->assign("BODY", formatText($pm['body'], $pm['fromID'], FORMAT_POST | $format));
    if($pm['toID'] == $userID) {
        $page->condition("CANREPLY", true);
        $page->assign("BOXID", $pm['boxID']);
    } else {
        $page->condition("CANREPLY", false);
        $page->assign("BOXID", 'outbox');
    }
    $screen->assign("BODY", $page->html());
    echo $screen->html();
    exit;
}

$error = '';
$disableTags = !empty($_REQUEST['disableTags']);
$disableSmilies = !empty($_REQUEST['disableSmilies']);
if(isset($_POST['send']) || isset($_POST['preview'])) {
    list($toID, $toRank) = $db->execute("SELECT userID, rank FROM {$prefix}_users WHERE username='".addslashes($_REQUEST['to'])."'")->fetchArray();
    if(!$toID)
        $error = $errors['mod']['no_such_user']."<br/>";
    if(empty($_REQUEST['subject']))
        $error .= $errors['post']['no_subject']."<br/>";
    if(empty($_REQUEST['body']))
        $error .= $errors['post']['no_body']."<br/>";
    if(isset($_POST['preview']) && $spellError)
        $error .= $errors['post']['spelling']; 
    if($toRank < 2 && $config['user']['rank'] < 1)
        $error .= $errors['post']['pm_banned'];
}

if(!$error && isset($_POST['send'])) {
    $query = "INSERT INTO {$prefix}_privmsg (fromID, toID, fromName, toName, subject, body, disableTags, disableSmilies, replyID, timestamp) 
              VALUES (%(fromID)s, %(toID)s, %(fromName)s, %(toName)s, %(subject)s, %(body)s, %(disableTags)s, %(disableSmilies)s, %(replyID)s, NOW())";
    $vars = array('fromID'=>$userID, 'toID'=>$toID, 'fromName'=>$config['user']['username'], 'toName'=>$_REQUEST['to'],
                  'subject'=>$_REQUEST['subject'], 'body'=>$_REQUEST['body'], 'disableTags'=>($disableTags ? 1 : 0),
                  'disableSmilies'=>($disableSmilies ? 1 : 0), 'replyID'=>(!empty($_REQUEST['replyID']) ? $_REQUEST['replyID'] : null));
    $db->execute($query, $vars);
    header("Location: pm.php?box=outbox");
    exit;
}

$screen = newPage($errors['local']['pm']);

if(isset($_POST['send']) || isset($_POST['preview']) || isset($_REQUEST['compose'])) {
    $page = new Template("pm.compose.html");
    $page->condition("PREVIEW", isset($_POST['preview']));
    if(!isset($_POST['send']) && !isset($_POST['preview']) && $config['user']['disableSmilies'])
        $disableSmilies = true;
    if(isset($_POST['preview'])) {
        $formatMode = ($disableTags ? FORMAT_NO_TAGS : 0) | ($disableSmilies ? FORMAT_NO_SMILIES : 0);
        $page->assign('PREVIEW', formatText($_REQUEST['body'], $userID, FORMAT_PREVIEW | $formatMode));
    }
    if(isset($_REQUEST['reply']) && ctype_digit($_REQUEST['reply'])) {
        $query = "SELECT u.username, p.subject, p.body, p.fromID
                  FROM {$prefix}_privmsg p INNER JOIN {$prefix}_users u ON p.fromID=u.userID
                  WHERE p.toID='{$userID}' AND p.msgID='{$_REQUEST['reply']}'";
        $reply = $db->execute($query)->fetchAssoc();
    } else {
        $reply = false;
    }
    $page->condition("REPLY", $reply);
    if($reply) {
        if(!isset($_REQUEST['to'])) $_REQUEST['to'] = $reply['username'];
        if(!isset($_REQUEST['subject'])) $_REQUEST['subject'] = 'Re: '.$reply['subject'];
        $format = ($pm['disableTags'] ? FORMAT_NO_TAGS : 0) | ($pm['disableSmilies'] ? FORMAT_NO_SMILIES : 0);
        $page->assign("REPLY", formatText($reply['body'], $reply['fromID'], FORMAT_POST | $format));
        $page->assign("REPLYID", $_REQUEST['reply']);
    } elseif(isset($_REQUEST['compose']) && ctype_digit($_REQUEST['compose']) && intval($_REQUEST['compose']) > 0 && !isset($_REQUEST['to'])) {
        $_REQUEST['to'] = $db->execute("SELECT username FROM {$prefix}_users WHERE userID='{$_REQUEST['compose']}'")->fetchField();
    }
    $page->condition("ERROR", $error);
    $page->assign("ERROR", $error);
    $page->assign("TO", isset($_REQUEST['to']) ? $_REQUEST['to'] : '');
    $page->assign("SUBJECT", isset($_REQUEST['subject']) ? $_REQUEST['subject'] : '');
    $page->assign("BODY", isset($_REQUEST['body']) ? htmlspecialchars($_REQUEST['body'], ENT_QUOTES) : '');
    $page->condition("DISABLETAGS", $disableTags);
    $page->condition("DISABLESMILIES", $disableSmilies);
} else {
    if(is_numeric($_REQUEST['page'])) {
        $start = ($_REQUEST['page']-1) * $config['setup']['threadsPerPage']; 
        $page = $_REQUEST['page'];
    } else {
        $start = 0;
        $page = 1;
    }

    $page = new Template("pm.body.html");
    if(!isset($_REQUEST['box']) || (!ctype_digit($_REQUEST['box']) && $_REQUEST['box'] != 'outbox')) {
        $_REQUEST['box'] = -1;
    }
    $page->assign("CURRENTBOX", $_REQUEST['box']);

    $boxes = '';
    if($_REQUEST['box'] != -1)
        $boxes = "<option value='-1'>{$errors['local']['inbox']}</option>";
    foreach($db->execute("SELECT boxID, title FROM {$prefix}_boxes WHERE userID='$userID' ORDER BY title")->iterator() as $box) {
        if($box['boxID'] != $_REQUEST['box'])
            $boxes .= "<option value='{$box['boxID']}'>".htmlspecialchars($box['title'])."</option>";
    }
    $page->condition("MOVE", ($_REQUEST['box'] != 'outbox') && $boxes);
    $page->assign("MOVEBOXES", $boxes);
    $boxes .= "<option value='outbox'>{$errors['local']['outbox']}</option>"; 
    $page->assign("VIEWBOXES", $boxes);
        
    if($_REQUEST['box'] == 'outbox') {
        $count = "SELECT COUNT(*) FROM {$prefix}_privmsg WHERE fromID='$userID' AND fromDeleted='0'";
        $query = "SELECT msgID, CASE WHEN u.userID IS NULL THEN toName ELSE u.username END AS name, subject, timestamp, 1 AS isRead
                  FROM {$prefix}_privmsg p LEFT JOIN {$prefix}_users u ON p.toID=u.userID
                  WHERE fromID='$userID' AND fromDeleted='0' ";
        $page->condition("MOVE", false);
    } else {
        $count = "SELECT COUNT(*) FROM {$prefix}_privmsg WHERE toID='$userID' AND toDeleted='0' AND boxID='{$_REQUEST['box']}'";
        $query = "SELECT msgID, CASE WHEN u.userID IS NULL THEN fromName ELSE u.username END AS name, subject, timestamp, isRead
                  FROM {$prefix}_privmsg p LEFT JOIN {$prefix}_users u ON p.fromID=u.userID
                  WHERE toID='$userID' AND toDeleted='0' ";
    }
    $query .= "ORDER BY timestamp DESC LIMIT $start, ".$config['setup']['threadsPerPage'];

    $ct = $db->execute($count)->fetchField();
    $page->condition("PAGER", $ct >$config['setup']['threadsPerPage']);
    $page->assign("PAGERTOP", makePager('pm.php?box='.$_REQUEST['box'], $ct, $config['setup']['threadsPerPage'], $page, true)); 
    $page->assign("PAGERBOTTOM", makePager('pm.php?box='.$_REQUEST['box'], $ct, $config['setup']['threadsPerPage'], $page, true)); 

    $rows = '';
    foreach($db->execute($query)->iterator() as $row) {
        $line = new Template("pm.line.html");
        $line->assign("PMID", $row['msgID']);
        if($row['isRead']) {
            $status = 'icon-no-new';
            $statusAlt = $errors['local']['is_read']; 
        } else {
            $status = 'icon-new';
            $statusAlt = $errors['local']['is_unread']; 
        }
        $line->assign("STATUS", imageLink($status.'.gif', $statusAlt));
        $line->assign("SUBJECT", $row['subject']);
        $line->assign("MEMBER", $row['name']);
        $line->assign("DATE", formatTime($row['timestamp']));
        $rows .= $line->html();
    }
    $page->assign("ROWS", $rows);
}

$screen->assign("BODY", $page->html());
echo $screen->html();

?>
