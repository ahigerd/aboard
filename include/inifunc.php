<?php
/**
 * INI file parsing routines
 *
 * @author      Adam Higerd
 * @version     $Revision: 1.2 $
 */

/**
 * Parse an INI file
 * @param string filename
 * @return array Hierarchical associative array of keys
 */
function parseini($fn)
{
    if(!is_readable($fn)) {
        trigger_error("file $fn missing in parseini();", E_USER_WARNING);
        return array();
    }
    $array = file($fn);
    $ret = array();
    $heading = "";
    $row = array();
    foreach($array as $line) {
        $line = trim($line);
        if($line=="" || $line{0}==';') continue;
        if($line{0}=='[') {
            if(!empty($row)) $ret[$heading] = $row;
            $heading = substr($line,1,-1);
            $row = array();
            continue;
        }
        $line = explode('=',$line,2);
        $key = trim($line[0]);
        $value = trim($line[1]);
        $row[$key] = $value;
    }
    if($heading) $ret[$heading] = $row;
    return $ret;
}

/**
 * Write an INI file
 * @param string filename
 * @param array Hierarchical associative array of keys
 * @return void
 */
function writeini($fn, $array)
{
    $fd = fopen($fn, 'w');
    foreach($array as $section => $keys) {
        fwrite($fd, "[$section]\n");
        if(is_array($keys) && count($keys)!=0) {
            foreach($keys as $key => $value) {
                fwrite($fd, "$key=$value\n");
            }
        }
        fwrite($fd, "\n");
    }
    fclose($fd);
}
?>
