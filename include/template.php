<?php

/**
 * Very simple templating class
 */
class Template {
    protected $data = '';
    protected $tags = array();
   
    /**
     * PHP 4 constructor
     * @param string filename of the template
     */
    function Template($filename) {
        if(isset($GLOBALS['config']['theme']['templatePath']) && is_readable($GLOBALS['config']['theme']['templatePath'].'/'.$filename))
            $this->data = file_get_contents($GLOBALS['config']['theme']['templatePath'].'/'.$filename);
        else
            $this->data = file_get_contents('templates/'.$filename);
        $this->tags["{IMGDIR}"] = $GLOBALS['config']['theme']['imagePath'];
        $this->tags["{SITENAME}"] = $GLOBALS['config']['setup']['forumName'];
        if(!empty($GLOBALS['config']['setup']['groupID'])) {
            $this->tags["{GROUPID}"] = $GLOBALS['config']['setup']['groupID'];
            $this->tags["{GROUP?PARAM}"] = "?group=".$GLOBALS['config']['setup']['groupID'];
            $this->tags["{GROUP&PARAM}"] = "&group=".$GLOBALS['config']['setup']['groupID'];
        } else {
            $this->tags["{GROUPID}"] = 0;
            $this->tags["{GROUP?PARAM}"] = "";
            $this->tags["{GROUP&PARAM}"] = "";
        }
    }
    
    /**
     * PHP 5 constructor
     * @param string filename of the tmeplate
     */
    function __construct($filename) {
        $this->Template($filename);
    }

    /**
     * Assign a value to a {tag}
     * @param string tag name
     * @param string value
     * @return chainable
     */
    function assign($tag, $value) {
        $this->tags["{".$tag."}"] = $value;
        return $this;
    }
  
    /**
     * Remove a |tag?...| based on a conditional 
     * @param string tag name
     * @param bool remove all or just tag?
     * @return chainable
     */
    function condition($tag, $value) {
        if($tag[0] != '!') $this->condition("!$tag", !$value);
        while(($s=strpos($this->data,"|$tag?"))!==false) {
            $q = strpos($this->data,"?",$s);
            $e = $s;
            do {
                $e = strpos($this->data,"|",$e+1);
            } while($this->data[$e-1]=='\\');
            $this->data = substr_replace($this->data, $value?str_replace("\\|","|",substr($this->data,$q+1,$e-$q-1)):'', $s, $e-$s+1);
        }
        return $this;
    }

    /**
     * Outputs the template with tags applied
     * @return string HTML
     */
    function html() {
        $temp = str_replace(array_keys($this->tags), array_values($this->tags), $this->data);
        $temp = preg_replace("/<img ((?:(?!alt=)[^>])*)\/?>/U","<img $1 alt='' />",$temp);
        $temp = preg_replace("/<input ((?:(?!class=)[^>])*)(type=['\"]?(?:submit|reset|button)['\"]?)((?:(?!class=)[^>])*)\/?>/U",
            "<input class='button' $1$2$3 />",$temp);
        $temp = preg_replace("/<input ((?:(?!class=)[^>])*)(type=['\"](?:text|password)['\"])((?:(?!class=)[^>])*)\/?>/U",
            "<input class='textfield' $1$2$3 />",$temp);
        $temp = preg_replace_callback("/<a.*href=[^>]*>/U",
            create_function('$matches','return preg_replace("/&(?!amp;)/","&amp;",$matches[0]);'),$temp);
        return $temp;
    }
};

?>
