<?php
//echo "<b>Green Maw is temporarily down for maintenance.</b>";
//exit;

/**
 * Configuration core.
 *
 * This file makes no assumptions as to the state of
 * the system and is safe to include anywhere.
 */

// Always ask Internet Explorer to use the latest-and-greatest rendering mode.
// Microsoft says not to use "edge" mode for production code, but since ABoard should generally not
// rely on browser-specific hacks, any regressions introduced by this are Microsoft's fault.
header("X-UA-Compatible: IE=edge");

include_once('include/inifunc.php');

define('VERSION','2.0a1');
define('COPYRIGHT', 'ABoard '.VERSION.' by Adam Higerd<br/>Copyright &copy; 2003-2010 Alkahest File Enterprises');

$config = array();

if(is_readable("aboard2.ini")) $config = parseini("aboard2.ini");

if(!isset($config['db']['host'])) $config['db']['host'] = 'localhost';
if(!isset($config['db']['database'])) $config['db']['database'] = 'aboard';
if(!isset($config['db']['username'])) $config['db']['username'] = '';
if(!isset($config['db']['password'])) $config['db']['password'] = '';
if(!isset($config['aboard']['version'])) $config['aboard']['version'] = 'none';
if(!isset($config['aboard']['prefix'])) $config['aboard']['prefix'] = 'aboard';

$config['aboard']['offset'] = -strtotime(gmdate("Y-m-d H:i:s", 0)) + 3600*date('I');
$_NOW = gmdate('Y-m-d H:i:s', time()+$config['aboard']['offset']);
// Sanitize input if magic quotes are turned on; life is much more consistent this way
function sanitize($val) { if(is_array($val)) return array_map("sanitize", $val); else return stripslashes($val); }
if(ini_get("magic_quotes_gpc")) {
    $_REQUEST = sanitize($_REQUEST);
    $_POST = sanitize($_POST);
    $_GET = sanitize($_GET);
    $_COOKIE = sanitize($_COOKIE);
}

?>
