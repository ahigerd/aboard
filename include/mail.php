<?php
/**
 * Sends an e-mail to a user
 * @param array list of user IDs
 * @param string subject line
 * @param string message body
 * @return bool success
 */
function sendMail($userID, $subject, $message) {
    global $db, $prefix, $config;
    if(!is_array($userID)) $userID = array($userID);
    foreach($db->execute("SELECT DISTINCT email FROM {$prefix}_users WHERE userID IN (".implode(",",$userID).")")->iterator() as $user)
        $to[] = $user['email'];
    $from = "From: {$config['setup']['forumName']} <{$config['setup']['adminEmail']}>";
    if($config['setup']['smtpHost']=='localhost')
        return mail(implode(", ", $to), $subject, $body, $from);
    return sendMailSMTP(implode(", ", $to), $config['setup']['adminEmail'], $subject, $body, $from);
}

/**
 * Sends an e-mail via SMTP
 * @param string to address
 * @param string from address
 * @param string subject line
 * @param string message body
 * @param string optional additional headers
 * @return bool success
 */
function sendMailSMTP($to,$from,$subject,$body,$headers="")
{
	global $config, $smtperr;

	$smtperr="5xx Unknown error";
	$fp = fsockopen($config['setup']['smtpHost'],$config['setup']['smtpPort'],$errno,$errmsg);
	if(!$fp) {
		$smtperr="520 Could not establish connection ( ".$errno." ".$errmsg." )";
		return 0;
	}

	$result = fgets($fp,1024);
	if($result{0}!="2") {
		$smtperr=$result;
		fputs($fp,"QUIT\r\n");
		fclose($fp);
		return 0;
	}
	while($result{3}=='-') {
		$result=fgets($fp,1024);
	}

	fputs($fp,"HELO $_SERVER[SERVER_NAME]\r\n");
	$result = fgets($fp,1024);
	if($result{0}!="2") {
		$smtperr=$result;
		fputs($fp,"QUIT\r\n");
		fclose($fp);
		return 0;
	}
	while($result{3}=='-') $result=fgets($fp,1024);

	if($config['setup']['smtpUser']) {
	    fputs($fp,"AUTH LOGIN\r\n");
	    fputs($fp,base64_encode($config['setup']['smtpUser'])."\r\n");
	    fputs($fp,base64_encode($config['setup']['smtpPass'])."\r\n");
		do { 
			$result=fgets($fp,1024);
		} while($result{0}=='3');
		if($result{0}!="2") {
			$smtperr=$result;
			fputs($fp,"QUIT\r\n");
			fclose($fp);
			return 0;
		}
	}

	fputs($fp,"MAIL FROM: $from\r\n");
	$result = fgets($fp,1024);
	if($result{0}!="2") {
		$smtperr=$result;
		fputs($fp,"QUIT\r\n");
		fclose($fp);
		return 0;
	}
	while($result{3}=='-') $result=fgets($fp,1024);
	fputs($fp,"RCPT TO: $to\r\n");
	$result = fgets($fp,1024);
	if($result{0}!="2") {
		$smtperr=$result; 
		fputs($fp,"QUIT\r\n");
		fclose($fp);
		return 0;
	}
	while($result{3}=='-') $result=fgets($fp,1024);
	fputs($fp,"DATA\r\n");
	$result = fgets($fp,1024);
	if($result{0}!="3") {
		$smtperr=$result;
		fputs($fp,"QUIT\r\n");
		fclose($fp);
		return 0;
	}
	while($result{3}=='-') $result=fgets($fp,1024);
	fputs($fp,"To: $to\r\n");
	fputs($fp,"Subject: $subject\r\n");
	if($headers) fputs($fp,"$headers\r\n");
	fputs($fp,"\r\n$body");
	fputs($fp,"\r\n.\r\n");
	$result = fgets($fp,1024);
	if($result{0}!="2") {
		fputs($fp,"QUIT\r\n");
		fclose($fp);
		return 0;
	}
	$smtperr="250 Success";
	fputs($fp,"QUIT\r\n");
	fclose($fp);
	return 1;
}
?>
