<?php
/**
 * Configuration database
 *
 * This file should only be included when the core
 * has a valid database login, but contains a lot
 * more configuration information.
 */

// Load common includes
include_once('include/configCore.php');
include_once('include/mysql_db.php');
include_once('include/validate.php');
include_once('include/template.php');
include_once('include/functions.php');

// Initialize database connection
$db = new MyDB($config['db']['host'], $config['db']['username'], $config['db']['password'], $config['db']['database']);
try {
    $db->open();
    $db->setCharset("utf8");
} catch (Exception $e) {
    echo "<html><head><title>ABoard {VERSION}</title></head>";
    echo "<body>ABoard was unable to connect to the database.<br/><br/>";
    echo "Please contact the server administrator.</body></html>";
    die();
}

// Fetch global configuration
$prefix = $config['aboard']['prefix'];
$subdomain = str_replace("'", "", $_SERVER['HTTP_HOST']);
$config['setup'] = $db->execute("SELECT c.*, t.title AS themeName, IFNULL(t.cssFile, 'default.css') AS cssFile,
                                 IFNULL(t.imagePath, 'images') AS imagePath, IFNULL(t.templatePath, 'templates') AS templatePath
                                 FROM {$prefix}_config c
                                 LEFT JOIN {$prefix}_themes t ON c.defaultTheme=t.themeID OR subdomain='$subdomain'
                                 ORDER BY (CASE WHEN subdomain='$subdomain' THEN 2 WHEN c.defaultTheme=t.themeID THEN 1 ELSE 0 END) DESC
                                 LIMIT 1")->fetchAssoc();
if(empty($config['setup']['timeFormat']) || $config['setup']['timeFormat']=='')
    $config['setup']['timeFormat'] = "Y-m-d g:i:s A";
if(empty($config['setup']['dateFormat']) || $config['setup']['dateFormat']=='')
    $config['setup']['dateFormat'] = "Y-m-d";

$theme = array('themeName' => $config['setup']['themeName'], 'cssFile' => $config['setup']['cssFile'],
               'imagePath' => $config['setup']['imagePath'], 'templatePath' => $config['setup']['templatePath']);
foreach($theme as $key => $value)
    unset($config['setup'][$key]);

// Confirm user login
$userID = validateLogin();

if(isset($config['user']) && $config['user']['themeID']) {
    $userTheme = $db->execute("SELECT * FROM {$prefix}_themes WHERE themeID=".$config['user']['themeID'])->fetchAssoc();
    if($userTheme) $theme = $userTheme;
}

setTheme($theme);

?>
