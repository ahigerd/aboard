<?php

/**
 * Store a cookie in the browser
 * @param string cookie name
 * @param string cookie value
 * @return void
 */
function storeCookie($name, $value) {
    setcookie($name, $value, time()+365*24*3600);
    $GLOBALS['_COOKIE'][$name] = $value;
}

/**
 * Clear a cookie on the browser
 * @param string cookie name
 * @return void
 */
function clearCookie($name) {
    setcookie($name, false, time()-1);
    unset($GLOBALS['_COOKIE'][$name]);
}

/**
 * Compares two Linux-style version numbers a la strcmp()
 * Note that Windows-style version numbers won't compare happily this way
 * but can be compared with a normal floating-point comparison
 * @param string The first version stamp
 * @param string The second version stamp
 * @return int -1 if first<second, +1 if first>second, 0 if first==second
 */
function vercmp($lhs, $rhs) {
    $ver1 = explode(".",$lhs);
    $ver2 = explode(".",$rhs);
    $len = max(count($ver1),count($ver2));
    for($i=0;$i<$len;$i++) {
        if((int)$ver1[$i]==(int)$ver2[$i]) continue;
        return ((int)$ver1[$i]<(int)$ver2[$i])?-1:1;
    }
    return 0;
}

/**
 * Create a template object for a new page and populate headers and footers
 * @param string Page title
 * @return object Template object containing the page
 */
function newPage($title = false) {
    global $config;
    if($title===false)
        $title = $GLOBALS['config']['setup']['forumName'];
    else
        $title = $GLOBALS['config']['setup']['forumName'].' - '.$title;
    $screen = new Template("body.html");
    $screen->assign("CSSFILE", $config['theme']['cssFile']);
    $screen->assign("IMGDIR", $config['theme']['imagePath']);
    $screen->assign("TITLE", $title);
    $screen->assign("COPYRIGHT", COPYRIGHT);
    $screen->assign("TIME", formatTime($GLOBALS['_NOW']));
    $screen->assign("FORUMDROP", makeForumDrop("forumID", "style='font-size:90%'"));
    if($config['user']['userID']) {
        $user = $config['user']['username'];
        switch($config['user']['rank']) {
            case 0:  $buttons = 'guest'; break;
            case 1:  $buttons = 'user';  break;
            case 4:  $buttons = 'admin'; break;
            default: $buttons = 'mod';   break;
        }
    } else {
        $buttons = 'guest';
        $user = '';
    }
    $buttons = new Template("buttons.$buttons.html");
    if($user=='')
        $login = new Template("login.guest.html");
    else
        $login = new Template("login.user.html");
    $login->condition("REGISTER", !$config['setup']['regDisable']);
    $login->condition("ADMIN", $config['user']['rank'] >= 4);
    $login->condition("MOD", $config['user']['rank'] >= 2);
    $login->assign("USERNAME", $user);

    // support putting the unread-messages indicator in any of the header templates
    $buttons->condition("UNREAD", $config['user']['pms'] > 0);
    $buttons->assign("UNREAD", $config['user']['pms']);
    $login->condition("UNREAD", $config['user']['pms'] > 0);
    $login->assign("UNREAD", $config['user']['pms']);
    $screen->condition("UNREAD", $config['user']['pms'] > 0);
    $screen->assign("UNREAD", $config['user']['pms']);

    $screen->assign("LOGIN", $login->html());
    $screen->assign("BUTTONS", $buttons->html());
    return $screen;
}

/**
 * Formats a timestamp
 * @param mixed timestamp, as a UNIX timestamp or a MySQL time string
 * @return string formatted time
 */
function formatTime($time = false, $format = false) {
    global $config;
    if($format===false) $format = $config['setup']['timeFormat'];
    if($time===false) $time = time();
    if(!is_numeric($time)) // not a timestamp
        $time = strtotime($time);
    $time -= $config['aboard']['offset'];
    if(isset($config['user']['timeZone'])) {
        $offset = 3600 * $config['user']['timeZone'];
        $useDst = $config['user']['useDst'];
    } else {
        $offset = $config['setup']['timeZone'] * 3600;
        $useDst = $config['setup']['useDst'];
    }
    $offset += 3600*$useDst*date("I", $time + $offset);
    return str_replace(" ","&nbsp;",date($format, $time + $offset));
}

/**
 * Formats a timestamp using the date format
 * @param mixed timestamp, as a UNIX timestamp or a MySQL time string
 * @return string formatted time
 */
function formatDate($time = false) {
    global $config;
    return formatTime($time, $config['setup']['dateFormat']);
}

/**
 * Generate a pager
 * @param string base URL
 * @param int number of items to be paged
 * @param int number of items per page
 * @param int current page number
 * @param bool toggle expanded view
 * @return string HTML pager
 */
function makePager($target, $count, $perPage, $current = 0, $expand = false) {
    static $elideCount = 0;
    $elideCount++;
    $elided = false;
    if($count <= $perPage) return '';
    if(strpos($target, '?')===false) $target .= '?page='; else $target .= '&page=';
    $pages = ceil($count / $perPage);
    $retval = 'Page: ';
    if($pages <= 10) {
        for($i=1; $i<=$pages; $i++) {
            if($i != $current)
                $retval .= "<a href='{$target}{$i}'>$i</a> ";
            else
                $retval .= $i . " ";
        }
    } else {
        $start1 = ($current<6)?max(4,$current+2):4;
        $end1 = ($current>5 && $current<=$pages-2)?$current-2:$pages-3;
        $start2 = ($end1<$pages-3)?$current+2:-1;
        $end2 = ($start2==-1)?-1:$pages-3;
        if($start1==$end1) {
            $start1 = $start2;
            $end1 = $end2;
            $start2 = $end2 = -1;
        } elseif($start2>$end2 || $start2==$end2) $start2 = $end2 = -1;
        for($i=1; $i<=$pages; $i++) {
            if($i==$current) {
                $retval .= $i." ";
            } elseif($start1==$i || $start2==$i) {
                if($expand)
                    $retval .= "<script language='javascript'><!--\nmakeElide('$elideCount');\n--></script> ";
                else
                    $retval .= "... ";
            } elseif(!($start1<$i && $i<=$end1) && !($start2<$i && $i<=$end2)) {
                $retval .= "<a href='{$target}{$i}'>$i</a> ";
            }
        }
        if($expand) {
            $retval = "<span id='elidelink$elideCount'>$retval</span><div id='elide$elideCount' class='elide' style='left-margin: 5em'>Page: ";
            for($i=1; $i<=$pages; $i++) {
                if($i==$current)
                    $retval .= $i." ";
                else
                    $retval .= "<a href='{$target}{$i}'>$i</a> ";
                if($i<$pages && $i%20==0) $retval .= "<br/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";
            }
            $retval .= "</div>";
        }
    }
    return $retval;
}

/**
 * Generate an <img> tag
 * @param string filename of the image
 * @param string ALT/TITLE text
 * @return string <img> tag with appropriate path specified
 */
function imageLink($filename, $alt = false) {
    global $config;
    if(!$alt) $alt = $filename;
    $alt = htmlspecialchars($alt, ENT_QUOTES);
    if(strtolower(substr($filename,0,4))=='http')
        return "<img src='$filename' alt='$alt' title='$alt' />";
    elseif(is_readable($config['theme']['imagePath'].'/'.$filename))
        return "<img src='{$config['theme']['imagePath']}/$filename' alt='$alt' title='$alt' />";
    else
        return "<img src='images/$filename' alt='$alt' title='$alt' />";
}

/**
 * Generate a verification string
 * @param string An identifying key
 * @param int Hash shift
 * @return string the verification string
 */
function verifyString($key, $shift=0) {
    global $db, $prefix;
    $chars = '2@3#45%6^7&8*9()wertupPUYTREWQASDFGHJKLkhgfdsazxcvbnmMNBVCXZ=?';
    $secret = $db->execute("SELECT secret FROM {$prefix}_keys WHERE code='$key'")->fetchField();
    if($secret=='') {
        $secret = md5(rand().":verifyImage:".time());
        $db->execute("INSERT INTO {$prefix}_keys SET code='$key', secret='$secret', created=NOW()");
    }
    $db->execute("DELETE FROM {$prefix}_keys WHERE created<NOW() - INTERVAL 24 HOUR");
    $hash = md5("verifyImage:$key:$secret", true);
    $str = "";
    for($i=$shift; $i<$shift+6; $i++) $str .= $chars[ord($hash[$i%16])%62];
    return $str;
}


function unichr($ord) {
    $msb = floor($ord / 256);
    $lsb = ($ord % 256);
    if($msb > 0) return chr($msb).chr($lsb);
    return chr($lsb);
}
/**
 * Formats a block of text with bbcode
 * @param string block of text
 * @param int ID of poster
 * @param enum type of formatting
 * @return html formatted text
 */
define("FORMAT_POST", 0);
define("FORMAT_SIG", 1);
define("FORMAT_PREVIEW", 2);
define("FORMAT_MODE_MASK", 3);
define("FORMAT_NO_TAGS", 4);
define("FORMAT_NO_SMILIES", 8);
function formatText($text, $posterID = false, $mode = FORMAT_POST) {
    global $config, $db, $userID, $prefix, $errors;
    static $tags = false;
    if(!$tags) {
        // Predefined tags here are those that aren't sensitive to restrictions
        $tags = array("/\[i\](.*)\[\/i\]/Umsi"=>"<i>$1</i>", "/\[b\](.*)\[\/b\]/Umsi"=>"<b>$1</b>",
            "/\[s\](.*)\[\/s\]/Umsi"=>"<s>$1</s>", "/\[u\](.*)\[\/u\]/Umsi"=>"<u>$1</u>",
            "/\[spoiler\](.*)\[\/spoiler\]/Umsi"=>"<div class='spoilheader'>{$errors['bbcode']['spoiler']}<div class='spoiler'>$1</div></div>",
            "/\[quote\](.*)\[\/quote\]/Umsi"=>"<div class='quoteheader'>{$errors['bbcode']['quote']}</div><div class='quote'>$1</div>",
            "/\[quote=([^\]]*)\](.*)\[\/quote\]/Umsi"=>"<div class='quoteheader'>{$errors['bbcode']['wrote']}</div><div class='quote'>$2</div>",
            "/\[img\](.*)\[\/img\]/Umsi"=>"<img src='$1' />",
        );
    }
    
    if(strpos($text, "&#") !== false)
        $text = mb_convert_encoding($text, 'UTF-8', 'HTML-ENTITIES');
    if(($mode & FORMAT_MODE_MASK) == FORMAT_PREVIEW) {
        $text = spellCheckBlock(htmlspecialchars($text)); 
        $mode &= ~FORMAT_POST;
    } else {
        $text = htmlspecialchars($text);
    }
    if(!($mode & FORMAT_NO_TAGS)) {
        do {
            $rv = $text;
            $text = preg_replace(array_keys($tags), array_values($tags), $rv);
        } while ($rv != $text);
    } else {
        $rv = $text;
    }
    return nl2br($rv);
}

/**
 * Generate a forum selector
 * @param string CSS ID; if contains [], renders as multiple-select
 * @param string HTML attribute string
 * @return html forum selector
 */
function makeForumDrop($id = "forumDrop", $attr = '') {
    global $db, $userID, $prefix;
    $multiple = strpos($id, '[]')!==false;
    $retval = '';
    $count = 0;
    $query = "SELECT f.forumID, f.name FROM {$prefix}_forums f LEFT JOIN {$prefix}_access a ON (a.userID='$userID' AND a.forumID=f.forumID) ";
    $query .= "WHERE ((a.actingRank IS NULL AND f.minimumRead<='{$config['user']['rank']}') OR (a.actingRank>=0)) AND f.isCategory='0' AND f.groupID IS NULL ";
    $query .= "ORDER BY sortOrder ASC";
    foreach($db->execute($query)->iterator() as $forum) {
        $count++;
        $retval .= "<option value='$forum[forumID]'>$forum[name]</option>"; 
    }
    return "<select $attr id='$id' name='$id' ".($multiple?'multiple="true" size="'.min($count,8).'"':'').">".$retval.'</select>';
}

/**
 * Spell check a word
 * @param resource pspell dictionary
 * @param string word to spell-check
 * @return html word, possibly with suggestions
 */
function spellCheckWord($dict, $word) {
    global $spellError;
    static $extraDict = false;
    // TODO: make this configurable in the admin section
    if(!$extraDict) $extraDict = array('img', 'jpg', 'png', 'bmp', 'gif', 'jpeg');
    $normal = strtolower($word);
    if(in_array($normal, $extraDict)) return $word;
    if(!pspell_check($dict, $normal) && !pspell_check($dict, str_replace("-","",$normal))) {
        $suggest = array();
        $dist = array();
        foreach(pspell_suggest($dict, $normal) as $s) {
            if(in_array($s, $suggest)) continue;
            if(strpos($s, '-')===false)
                $l = levenshtein(str_replace("-","",$normal), strtolower($s));
            else
                $l = levenshtein($normal, strtolower($s));
            if($l==0) return $word; 
            if($l>3) continue;
            $suggest[] = $s;
            $dist[] = $l;
        }
        $spellError = true;
        if(empty($suggest))
            $suggest = array("<i>No suggestions</i>");
        else
            array_multisort($dist, $suggest);
        return "<a href='#' class='suggest'>$word<span>".implode(", ", $suggest)."</span></a>"; 
    } else {
        return $word;
    }
}

/**
 * Spell check a block of text
 * @param string block of text to spell check
 * @return html text block, possibly with suggestions
 */
function spellCheckBlock($text) {
    if(!function_exists('pspell_config_create')) {
        // pspell unavailable
        return $text;
    }
    global $errors, $spellError;
    $retval = '';
    $cfg = pspell_config_create($errors['local']['pspell_dict'], '', '', 'utf-8');
    pspell_config_mode($cfg, PSPELL_FAST);
    pspell_config_runtogether($cfg, true);
    $dict = pspell_new_config($cfg);
    $spellError = false;
    foreach(preg_split("/((?<=[0-9A-Za-z'-])(?=[^0-9A-Za-z'-])|(?<=[^0-9A-Za-z'-])(?=[0-9A-Za-z'-]))/", $text) as $word) {
        if(!preg_match("/^[A-Za-z'-]{3,}$/", $word))
            $retval .= htmlspecialchars($word);
        else
            $retval .= spellCheckWord($dict, $word);
    }
    return $retval;
}

/**
 * Updates the configuration based on the selected group.
 * If the specified group does not exist, this function does nothing. 
 * @param int group ID
 * @param array pre-queried data, optional
 * @return void
 */
function setGroup($groupID, $data = false) {
    global $config, $errors, $db, $prefix;
    if(!ctype_digit($groupID)) return;
    if(!$data) {
        $query = "SELECT g.name AS groupName, g.themeID, t.title AS themeName, t.*, IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank
                  FROM {$prefix}_groups g 
                  INNER JOIN {$prefix}_themes t ON g.themeID=t.themeID
                  LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$config['user']['userID']}'
                  WHERE g.groupID={$groupID}";
        $data = $db->execute($query)->fetchAssoc();
    }
    if(!$data) return;
    if($config['user']['realRank'] < 4)
        $config['user']['rank'] = $data['rank'];
    $config['setup']['groupID'] = $groupID;
    $config['setup']['forumName'] .= ' - '.$data['groupName'];
    if($data['themeName']) setTheme($data);
}

/**
 * Updates the configuration to use the specified theme. The theme must exist.
 * @param array Theme configuration. Required keys: themeName, cssFile, imagePath, templatePath
 * @return void
 */
function setTheme($data) {
    global $config, $errors;
    $config['theme'] = array('themeName' => $data['themeName'], 'cssFile' => $data['cssFile'],
                             'imagePath' => $data['imagePath'], 'templatePath' => $data['templatePath']);
    if(is_readable($config['theme']['templatePath'].'/errors.ini'))
        $errors = parseini($config['theme']['templatePath'].'/errors.ini');
    else
        $errors = parseini('templates/errors.ini');
}

/**
 * Updates the configuration for the group specified by the forum ID.
 * @param int forum ID
 * @param array pre-queried data, optional
 * @return void
 */
function setGroupByForumID($forumID, $data = false) {
    global $config, $db, $prefix;
    if(!ctype_digit($forumID)) return;
    if(!$data) {
        $query = "SELECT f.groupID, g.themeID, g.name AS groupName, t.title AS themeName, t.*, IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank
                  FROM {$prefix}_forums f
                  INNER JOIN {$prefix}_groups g ON f.groupID=g.groupID
                  INNER JOIN {$prefix}_themes t ON g.themeID=t.themeID
                  LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$config['user']['userID']}'
                  WHERE f.forumID={$forumID}";
        $data = $db->execute($query)->fetchAssoc();
    }
    if(!$data) return;
    setGroup($data['groupID'], $data);
}

/**
 * Updates the configuration for the group specified by the thread ID.
 * @param int thread ID
 * @param array pre-queried data, optional
 * @return void
 */
function setGroupByThreadID($threadID, $data = false) {
    global $config, $db, $prefix;
    if(!ctype_digit($threadID)) return;
    if(!$data) {
        $query = "SELECT f.groupID, g.themeID, g.name AS groupName, t.title AS themeName, t.*, IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank
                  FROM {$prefix}_threads th
                  INNER JOIN {$prefix}_forums f ON th.forumID=f.forumID
                  INNER JOIN {$prefix}_groups g ON f.groupID=g.groupID
                  INNER JOIN {$prefix}_themes t ON g.themeID=t.themeID
                  LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$config['user']['userID']}'
                  WHERE th.threadID={$threadID}";
        $data = $db->execute($query)->fetchAssoc();
    }
    if(!$data) return;
    setGroup($data['groupID'], $data);
}

/**
 * Updates the configuration for the group specified by the post ID.
 * @param int post ID
 * @param array pre-queried data, optional
 * @return void
 */
function setGroupByPostID($postID, $data = false) {
    global $config, $db, $prefix;
    if(!ctype_digit($postID)) return;
    if(!$data) {
        $query = "SELECT f.groupID, g.themeID, g.name AS groupName, t.title AS themeName, t.*, IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank
                  FROM {$prefix}_posts p
                  INNER JOIN {$prefix}_threads th ON p.threadID=th.threadID
                  INNER JOIN {$prefix}_forums f ON th.forumID=f.forumID
                  INNER JOIN {$prefix}_groups g ON f.groupID=g.groupID
                  INNER JOIN {$prefix}_themes t ON g.themeID=t.themeID
                  LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$config['user']['userID']}'
                  WHERE p.postID={$postID}"; 
        $data = $db->execute($query)->fetchAssoc();
    }
    if(!$data) return;
    setGroup($data['groupID'], $data);
}
