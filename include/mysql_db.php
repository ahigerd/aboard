<?php
include_once("SQLException.php");
/***************************************************************************
 *   Copyright (C) 2004, 2005 by Dan Ostrowski                             *
 *   dan@ostrowski.cc                                                      *
 *                                                                         *
 *   Permission is hereby granted, free of charge, to any person obtaining *
 *   a copy of this software and associated documentation files (the       *
 *   "Software"), to deal in the Software without restriction, including   *
 *   without limitation the rights to use, copy, modify, merge, publish,   *
 *   distribute, sublicense, and/or sell copies of the Software, and to    *
 *   permit persons to whom the Software is furnished to do so, subject to *
 *   the following conditions:                                             *
 *                                                                         *
 *   The above copyright notice and this permission notice shall be        *
 *   included in all copies or substantial portions of the Software.       *
 *                                                                         *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    *
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR     *
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, *
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR *
 *   OTHER DEALINGS IN THE SOFTWARE.                                       *
 ***************************************************************************/

/**
 * MySQL Database Abstraction Objects
 *
 * Usage:
 * 
 * $db = &new MyDB( 'host','user','password','database' );
 * // run a query
 * if( $db->query( 'select * from users' ) ) {
 *      while( $row = $db->fetchAssoc( ) ) {
 *              // do something with row array
 *      }
 * } else {
 *      print "Query failed! " . $db->error( );
 * }
 *
 * // update every row in a table where field1 = 2
 * if( $db->update( 'mytable', 
 *
 * @author Dan Ostrowski <dan@ostrowski.cc>
 */

/**
 * Iterator type class that holds result resources.
 *
 * This is used in situations where you might want to use two result resources at the same
 * time. Mostly this occurrs in situations where you query, loop through the result set 
 * and call out to functions during the looping that might also need database resources. 
 *
 * Using the MyDB {@see MyDB::getIter()} function you can spawn this iterator class and then use it
 * independandly of the database object.
 *
 * This class forms the base class for the larger MyDB object which is a superset of the functionality
 * in the iterator class.
 */
class MyDbIter implements Iterator
{
    /**
     * Result resource.
     * @access private
     */
    protected $res;

    /** 
     * Identifying string.
     * 
     * This type of database shouldn't make too much difference. One of the main
     * uses of this class is ABSTRACTION so only very rarely should you be checking
     * what actual type of database it is. It shouldn't matter.
     * @access protected
     */
    protected $id = "my";

    /**
     * Value of the current element.
     *
     * Mostly this is important for iteration.
     */
    protected $current = null;

    /**
     * Current key.
     *
     * Mostly used in iteration. This will actually
     * only be called when the foreach construct calls
     * for a key and is usually unimportant.
     */
    protected $key = 0;

    /**
     * Counter to keep track of the current key.
     *
     * This gets incremented after {@link MyDbIter::next} and other functions.
     */
    protected $counter = 0;

    /**
     * PHP 4 Constructor.
     *
     * Accepts a result resource.
     *
     * @param resource query result resource
     */
    function MyDbIter( $res )
    {
        $this->__construct( $res );
    }

    /**
     * PHP 5 Constructor.
     *
     * Accepts a result resource.
     *
     * @param resource query result resource
     */
    function __construct( $res )
    {
        if( is_resource( $res ) ) {
            $this->res = $res;
        }
        else $this->res = false;
    }

    function __destruct( ) 
    {
        $this->freeResult();
    }

    /**
     * Returns an identifying string
     *
     * Note: This function should be used sparingly as one of
     * the main purposes of this class is ABSTRACTION. The underlying
     * database engine should, theoretically at least, make little difference.
     *
     * @return string
     */
    function ID()
    {
        return $this->id;
    }

    /**
     * Returns the number of rows for the current result set.
     *
     * Usage mirrors {@see mysql_num_rows function}.
     * @result int number of rows.
     */
    function numRows()
    {
        if( is_resource( $this->res ) )
            return(@mysql_num_rows($this->res));
        else return 0;
    }

    /**
     * Returns an object representing one row of a result set.
     * 
     * Mirrors {@see mysql_fetch_object function}.
     * @return mixed
     */
    function fetchObj() 
    {
        return clone $this->__fetch( 'mysql_fetch_object' );
    }

    /**
     * Fetches a field from a result set.
     * @param int the field number to fetch from the result set
     * @param mixed string,int or other scalar
     */
    function fetchField( $i = 0 )
    {
        $tmp = $this->fetchArray( );
        if( !$tmp ) return false;
        if( isset( $tmp[$i] ) ) return $tmp[$i];
        return null;
    }

    /**
     * Fetches an array representing a row of the current result set.
     * 
     * Mirrors {@see mysql_fetch_array function}.
     * @return mixed
     */
    function fetchArray( )
    {
        return $this->__fetch( 'mysql_fetch_array' );
    }

    /**
     * Fetches an array representing a row of the current result set.
     * 
     * Mirrors {@see mysql_fetch_array function}.
     * @return mixed
     */
    function fetchAssoc()
    {
        return $this->__fetch( 'mysql_fetch_assoc' );
    }

    function __fetch( $function )
    {
        $this->current = $function($this->res);
        $this->key = $this->counter;
        ++$this->counter;
        if( !$this->current) $this->freeResult( );
        return $this->current;
    }

    /**
     * Check if there is a current element after calls to {@link MyDbIter::rewind} or {@link MyDbIter::next}. 
     */
    function valid( )
    {
        if( is_resource( $this->res ) && $this->res && !$this->current ) {
            $this->next( );
        }
        return (bool) $this->res;
    }

    /**
     * Returns the current element.
     *
     * Inherited from {@link Iterator} interface.
     */
    function current( )
    {
        if( is_object( $this->current ) )
            return clone $this->current;
        return $this->current;
    }

    /**
     * Returns the key of the current element.
     */
    function key( )
    {
        return $this->key;
    }

    /**
     * Move forward to the next element.
     */
    function next( )
    {
        $this->fetchAssoc( );
    }

    /**
     * Rewinds the object. In this case it does nothing.
     */
    function rewind( )
    {
        // pass
    }

    /**
     * Explicitly frees the resources used by the current result set.
     *
     * Mirrors the functionality of {@see mysql_free_result}.
     * @return bool success
     */
    function freeResult()
    {
        /* on select, show, explain or describe, result will be a result resource.
           Otherwise, for updates and inserts and such, it will be a boolean true. 
           Don't want to try freeing the result resource if it's boolean true or a
           E_WARNING will be raised.
         */
        if( $this->res && is_resource( $this->res ) ) { 
            $return = @mysql_free_result($this->res);
            $this->res = null;
        }
        else $return = true;
        /* insert debugging here if needed */
        return $return;
    }

}


/**
 * The main query class.
 * 
 * This class holds the methods for inserting, deleting, updating and querying. It can also
 * store and iterate through one result set. Since more than one result set might be used at any one time,
 * this set can also spawn {@see dbIter class} Objects for storing and iterating through result sets.
 */
class MyDB extends MyDbIter
{
    /**
     * Holds the last query run (successful or otherwise)
     */
    public $last_query;

    /**
     * Holds the host we're connected to.
     * @access protected
     */
    protected $host = '';
    /**
     * Database username to connect with.
     * @access protected
     */
    protected $user = '';
    /**
     * Password to connect to the database with
     * @access protected
     */
    protected $password = '';
    /**
     * Database name
     * @access protected
     */
    protected $database = '';
    /**
     * Indicates a persistent connection. 
     *
     * With databases who cannot support this flag properly, it means nothing,
     * but MySQL is very capable of persistent connections
     * @access protected
     */
    protected $persistent = false;


    /**
     * Connection resource.
     * @access protected
     */
    protected $conn = NULL;

    /**
     * This flag determines whether or not null strings are treated as NULL in the database various places.
     *
     * Sometimes you may want '' strings to be searched for/inserted as nulls, and sometimes not.
     *
     * @see _build_where_clause function
     * @see setSloppyNullStrings function
     * @access protected
     */
    protected $sloppy_null_strings = true;

    /**
     * The port to connect to the mysql server on.
     */
    protected $port = null;

    /**
     * PHP 4 Constructor function.
     *
     * Of note is the persistent connection parameter at the end. This constructor simply
     * calls the PHP 5 constructor.
     *
     * @param string host to connect to. most sites will use 'localhost'.
     * @param string user to connect as
     * @param string password to identify with
     * @param string database to use
     * @param bool persistent connection or no?
     */
    function MyDB($host, $user, $password, $database, $persistent = false)
    {
        $this->__construct( $host, $user, $password, $database, $persistent );
    }

    /**
     * PHP 5 Constructor function.
     *
     * Of note is the persistent connection parameter at the end. 
     *
     * @param string host to connect to. most sites will use 'localhost'.
     * @param string user to connect as
     * @param string password to identify with
     * @param string database to use
     * @param bool persistent connection or no?
     */
    function __construct($host, $user, $password, $database, $persistent=false, $port=null)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
        $this->persistent = $persistent;
        $this->port = $port;
    }

    /**
     * Alias of {@link MyDB::getIter}
     * @return object {@link MyDbIter} object.
     */
    function iterator( ) { return $this->getIter( ); }

    /**
     * Returns an iterator object.
     *
     * @return object {@link MyDbIter} object.
     */
    function getIter( )
    {
        $ret = new MyDbIter( $this->res );
        $this->res = false;
        return $ret;
    }

    /**
     * PHP 5 Destructor.
     *
     * If this db object is not persistent, the connection will be closed. 
     * The objects result resource will always be freed.
     *
     * @return void
     */
    function __destruct( )
    {
        if( $this->res )
            $this->freeResult( );
        if( !$this->persistent )
            $this->close( );
    }



    /**
     * Opens a connection to the database.
     *
     * This method, while not private, is deprecated over the use of {@see confirmOpen function}
     * which first checks if a connection has been established and only then calls open. This prevents
     * multiple connections being opened and stops the "if not open, then open" drudgery that otherwise
     * would be needed 
     *
     * @return void 
     * @throws SQLException when connection fails.
     */
    function open()
    {
        if($this->isOpen())
            return $this;

        /* persistent connection, or no? */
        if ($this->persistent) $func = 'mysql_pconnect';
        else $func = 'mysql_connect';

        /* open the connection or die */
        $host = $this->host;
        if($this->port) {
            $host .= ':'.$this->port;
        }
        $this->conn = @$func($this->host, $this->user, $this->password);
        if(!$this->conn) {
            throw new SQLException( "Could not connect to database at {$this->host} as {$this->user}. Error: ".$this->error());
        }

        /* select database */
        if( ! @mysql_select_db($this->database, $this->conn) )
            throw new SQLException( "Could not select database {$this->database}. Error: ".$this->error( ) );
        return $this;
    }

    /**
     * Selects a database and on success sets the database property.
     *
     * @param string database name
     */
    function selectDb($database) 
    {
        if( ! @mysql_select_db($database, $this->conn) )
            throw new SQLException( "Could not select database $database. Error: ".$this->error( ) );
        $this->database = $database;
    }

    /**
     * Sets sloppy null string comparison. 
     * 
     * When true, null strings will be treated as NULLs in the database for
     * comparison and insertion purposes.
     * 
     * @param bool Null strings count as NULLs
     * @return void
     */
    function setSloppyNullStrings( $bool = true )
    {
        $this->sloppy_null_strings = $bool;
    }

    /**
     * Does "_sloppy_null_strings" aware null comparison
     *
     * @param mixed value to check
     * @return bool indicates value should be used/compared to as null in the database
     */
    function isNull( $value ) 
    {
        if( $value === null || ( $value === '' && $this->sloppy_null_strings ) )
            return true;
        return false;
    }

    /**
     * Closes the database connection if available.
     *
     * Note that persistent connections can't really be "closed" in most instances.
     *
     * @return bool success
     */
    function close()
    {
        if(!is_resource($this->conn)) return true;
        return(@mysql_close($this->conn));
    }

    /**
     * Sets the connection charset.
     */
    function setCharset($charset)
    {
        if(!is_resource($this->conn)) {
            throw new SQLException("Must be connected before setting character set.");
        }
        mysql_set_charset($charset, $this->conn);
    }

    /**
     * Returns the error that occurred with the current resource.
     *
     * @return string error message.
     */
    function error() 
    {
        return(@mysql_error($this->conn));
    }

    /**
     * Executes a query and stores the result in the Object.
     *
     * NOTE: this function is deprecated, the execute() function is the best way to run sql queries.
     *
     * @param string query
     * @return Object reference to self
     * @deprecated
     */
    function query($sql)
    {
        $this->res = mysql_query($sql, $this->conn);
        $this->last_query = $sql;
        if( $this->res === false ) {
            throw new SQLException("Illegal Query: [[ ".substr($sql,0,350)." ]] Error: ".$this->error());
        }
        else {
            return $this;
        }
    }

    /**
     * The number of rows affected by the last query executed.
     *
     * Usage mirrors {@see mysql_affected_rows function}.
     *
     * @return int number of rows
     */
    function affectedRows()
    {
        return(@mysql_affected_rows($this->conn));
    }

    /**
     * Executes a query, substituting values specified in an optional array.
     *
     * This follows the Pythonesque method of replacement. Specifying %(varname)s or %(varname)d will do
     * key based replacement and format the values into strings or decimals respectively. Using the
     * modulo version of interpolation will also escape strings if they need escaping. Specifying !(varname)s
     * will simply pass in raw the value of the variable contained in the array without escaping or altering the value at all.
     *
     * @param string containing query
     * @param array either a mapping of key=>values to be used or just a list
     * @todo finish optional array accepting list
     * @return Object reference to current instance of obj.
     */
    function execute( $query, $vars = null )
    {
        if( !$vars || !is_array($vars) ) {
            $this->query($query);
            return $this;
        }

        /* there's substitution to do potentially */
        $subjects = array( );           // we only want to call str_replace once
        $replacements = array( );
        $i = 0; // need to keep track of indexing as well.
        foreach( $vars as $key => $value ) {
            if( strpos( $query,"%($key)s" ) !== false ) {               // found %(key)s
                $subjects[] = "%($key)s";
                if( $this->isNull($value) ) {
                    $replacements[] = 'NULL';
                } else {
                    $replacements[] = "'".addslashes($value)."'";
                }
            } else if ( strpos( $query,"%($key)d" ) !== false ) {         // found %(key)d
                $subjects[] = "%($key)d";
                if( $this->isNull( $value ) ) {
                    $replacements[] = 'NULL';
                } else {
                    if( !is_numeric($value) ) 
                        throw new SQLException("Attempt to convert non-numeric value [[ $value ]] to a decimal.");
                    $replacements[] = strval($value);
                }
            } else if ( strpos( $query,"!($key)s" ) !== false ) {                   // found !(key)s
                $subjects[] = "!($key)s";
                if( $this->isNull( $value ) ) {
                    $replacements[] = 'NULL';
                } else {
                    $replacements[] = $value;
                }
            }
        }
        if( $subjects && is_array( $subjects ) ) {
            $query = str_replace($subjects,$replacements,$query);
        }
        return $this->query( $query );
    }

    /**
     * Determines if a connection is open.
     *
     * @return bool indicates connection established.
     */
    function isOpen()
    {
        return($this->conn);
    }

    /**
     * Private function to be used by {@see insert function} or {@see replace function}.
     *
     * It is not recommended to use replace. It is NOT a SQL standard and can be confusing.
     * 
     * @access private
     * @param string command value... either 'insert' or 'replace'.
     * @param string table name
     * @param array array of values to insert or replace.
     * @return bool success
     */
    private function _insert_or_replace($cmd,$table,$array)
    {
        $table = addslashes($table);

        $insert = array();  /* translated values to insert */
        foreach( $array as $key => $val ) {
            /* this first if logic is so that 0's go in as 0's,
             * but null strings go in as null */
            if( $this->isNull( $val ) )
                $insert[$key] = 'null';
            else if ( is_int( $val ) )
                $insert[$key] = $val;
            else 
                $insert[$key] = "'".addslashes($val)."'";
        }

        $query = "$cmd into $table (" . implode(",", array_keys($insert)) . ") values (" . implode(',', $insert) . ")";

        if(!$this->isOpen()) $this->open();

        return($this->query($query));
    }

    /**
     * Uses {@see _build_clause function} to build an assignment array.
     *
     * @access private
     * @param mixed key to use as identifier
     * @param mixed value to match identifier
     * @return array of associative sql statements
     */
    function _build_assignment_clause( $where, $equals )
    {
        return $this->_build_clause( $where, $equals, '=' );
    }

    /**
     * Uses {@see _build_clause function} to build a comparison array.
     *
     * @access private
     * @param mixed key to use as identifier
     * @param mixed value to match identifier
     * @return array of associative sql statements
     */
    function _build_compare_clause( $where, $equals )
    {
        return $this->_build_clause( $where, $equals );
    }

    /**
     * Private function to build a set of sql clauses out of mixed variables
     * 
     * @access private
     * @param mixed key to use as identifier
     * @param mixed value to match identifier
     * @param string seperate null values by this string. should be '=' or 'is' for assignment or comparison respectively. 
     * @return array of associative sql statements
     */
    function _build_clause( $where, $equals, $null_method = 'is' )
    {
        if( !eregi( '^(=|is)$', $null_method ) ) {
            $null_method = 'is';
        }

        if( is_array( $where ) && is_array( $equals ) ) {
            /* assume indexed arrays and "x=y AND x=y" (or OR) */
            $wherecount = count( $where );      // cache result
            $tmp = array( );
            for( $i=0; $i<$wherecount; ++$i ) {
                if( $this->isNull( $equals[$i] ) ) {
                    /* they really want null, not an empty element */
                    $tmp[] = "{$where[$i]} $null_method null";

                } else if ( is_scalar( $equals[$i] ) ) {
                    $equals[$i] = addslashes( $equals[$i] );
                    $tmp[] = "{$where[$i]} = '{$equals[$i]}'";

                } else {
                    continue;
                    /** {$todo throw exception here} */
                }

            }
            return $tmp;

        } else if ( is_scalar( $where ) && is_array( $equals ) ) {
            /* where x in (y1,y2,yn) */
            $equals = array_map( 'addslashes', $equals );

            $clause = "$where in ('" . implode( "','", $equals ) . "')";
            return array( $clause );

        } else if( is_array( $where ) && $equals === null ) {
            /* where x = y AND z = d, but $where is an associative array */
            $tmp = array( );
            foreach( $where as $key => $value ) {
                if( $this->isNull( $value ) ) {
                    $tmp[] = "$key $null_method null";

                } else if ( is_scalar( $key ) && is_scalar( $value ) ) {
                    $value = addslashes( $value );
                    $tmp[] = "$key = '$value'";

                } else {
                    continue;
                    /** {@todo throw exception} */
                }
            }
            return $tmp;

        } else if( is_scalar( $where ) && is_scalar( $equals ) ) {
            /* where x = y */
            $equals = addslashes( $equals );

            $clause = "$where = '$equals'";
            return array( $clause );

        } else {
            /** {@todo throw exception} */
            return array( );
        }
    }

    /**
     * Inserts an associative array into a table in the database.
     *
     * @todo this should be able to take anything iterable. ( this includes php5 objects )
     * @param string table to insert into
     * @param array associative array of fieldnames and corresponding values to insert into the table.
     * @return bool success
     */
    function insert($table, $array) 
    {
        return $this->_insert_or_replace( 'insert', $table, $array );
    }

    /**
     * Replaces an associative array into a table in the database.
     *
     * @todo this should be able to take anything iterable. ( this includes php5 objects )
     * @param string table to insert into
     * @param array associative array of fieldnames and corresponding values to replace into the table.
     * @return bool success
     */
    function replace( $table, $array ) 
    {
        return $this->_insert_or_replace( 'replace',$table, $array );
    }

    /**
     * Updates a table using key/values to identify the row to be updated.
     *
     * Note that where and equals could be two arrays, two strings or a string and an array.
     * If an array is passed for both, the and_or parameter will be used to determine how to link these arrays.
     * If a string is passed for both, it will be a simple where x = y.
     * If a string and an array are passed, it will be a where x in ( y[1],y[2],y[n] ) situation.
     *
     * @param string table to update
     * @param array associative array to update the table with
     * @param mixed the key to look for to update rows with.
     * @param mixed the value to match with the key and identify rows to be updated with.
     * @param string value to concatenate where and equals with.
     * @return bool success
     */
    function update($table, $array, $where = null, $equals = null, $and_or = 'AND')
    {
        $setclause = implode( ",", $this->_build_assignment_clause( $array, null ) );

        $query = "UPDATE $table SET $setclause";
        if( $where ) {
            $query .= " WHERE ".implode( " $and_or ", $this->_build_compare_clause( $where, $equals ) );
        }

        if(!$this->isOpen()) $this->open();

        return($this->query($query));
    }


    /**
     * Deletes rows from a table using a key value identification
     *
     * Where and Equals here follow the same rules as {@see update function}.
     *
     * @param string table to delete from
     * @param mixed key to use for deleting rows
     * @param mixed value to match key to use for identifying rows to be deleted
     * @param string concatenation to use between $where and $equals
     * @return bool query was executed successfully.
     */
    function del($table, $where=null, $equals = null, $and_or = 'AND')
    {
        $query = "DELETE FROM $table";
        if($where) {
            $whereclause = implode(" $and_or ", $this->_build_compare_clause($where, $equals));
            $query .= " WHERE $whereclause";
        }
        return($this->query($query));
    }

    /**
     * This function checks if a connection is established, and if not, opens one.
     * 
     * @return bool
     */
    function confirmOpen()
    {
        $this->open();
    }

    /**
     * Returns the current result set as an array
     *
     * @param string key to be used to determine which item to put in the array.
     * @return array list of either one item, if key is passed, or all items from result set in an array.
     */
    function getResultArray( $key = null )
    {
        $return = array( );
        while( $row = $this->fetchAssoc( ) ) {
            if( !$key ) {
                $return[] = $row;
            } else if( isset( $row[$key] ) ) {
                $return[] = $row[$key];
            }
        }
        return $return;
    }
}

?>
