<?php
include_once("mysql_db.php");

/**
 * Simple structure to store various field metadata
 */
class DatabaseField {
    public $dataType;
    public $default = false;
    public $nullable = false;
    public $properties = ''; // For dataType in (index, primary, fulltext, unique), contains an array
};

/**
 * Explodes a string into an array, watching for parenthesis matching
 * @param string separator to explode upon
 * @param string string to be exploded
 * @return array exploded string
 */
function smart_explode($separator, $string) {
    $array = explode($separator, $string);
    $retval = array();
    for($i=0;$i<count($array);$i++) {
        $str = $array[$i];
        while(substr_count($str,"(") > substr_count($str,")")) {
            $i++;
            if($i>count($array)) break;
            $str .= ',' . $array[$i];
        }
        $retval[] = $str;
    }
    return $retval;
}

/**
 * Check to see if two arrays contain the same elements, order-insensitive
 * @param array left-hand array
 * @param array right-hand array
 * @return bool true if arrays match
 */
function indexMatch($lhs, $rhs) {
    $count = count($rhs);
    foreach($lhs as $field) {
        if(!in_array($field, $rhs)) return false;
        $count--;
    }
    return ($count==0);
}

/**
 * Checks to see if two data types are equivalent
 * @param string one data type
 * @param string other data type
 * @return bool true if data types are equivalent
 */
function dataTypeMatch($lhs, $rhs) {
    static $from = array("INT(11)", "SMALLINT(6)", "TINYINT(4)", "TIMESTAMP(14)", "VARCHAR");
    static $to   = array("INTEGER", "SMALLINT",    "TINYINT",    "TIMESTAMP",     "CHAR");
    $lhs = str_replace($from, $to, strtoupper($lhs));
    $rhs = str_replace($from, $to, strtoupper($rhs));
    return $lhs == $rhs;
}

/**
 * Loads a database definition contained in a string
 * and parse it into a usable array format
 * @param string schema
 * @return array (table name => array (field name => DatabaseField)) describing schema
 */
function loadSchemaFromString($schema) {
    $retval = array();
    $schema = smart_explode(";", $schema);
    
    foreach($schema as $table) {
        $fields = array();
        $idxCounter = 0;

        $table = trim(str_replace("\n","",str_replace("\r","",str_replace("\t"," ",preg_replace("/--[^\n]*\n/","",$table)))));
        if(!$table) continue;
        if(!preg_match("/CREATE TABLE *([^ (]+) *\\((.*)\\) *(?:(?:TYPE|ENGINE) *=.*)?/i", $table, $matches)) {
            echo "preg_match didn't work";
            return false;
        }
        if(preg_match("/DEFAULT CHARSET *= *([^ ]+)/", $table, $charsetMatches)) 
            $fields['DEFAULT CHARSET'] = $charsetMatches[1];
        $tableName = str_replace("'","",str_replace('"','',str_replace("`","",$matches[1]))); 
        foreach(smart_explode(",",$matches[2]) as $field) {
            $field = trim($field);
            if(!$field) continue;
            $fieldInfo = new DatabaseField();
            if(stripos($field,"UNIQUE")===0 || stripos($field,"INDEX")===0 || stripos($field,"FULLTEXT")===0 ||
               stripos($field,"KEY")===0 || stripos($field,"PRIMARY")===0) {
                if(!preg_match("/(PRIMARY|UNIQUE|FULLTEXT)? *(?:KEY|INDEX)? *([^ ]+)? *\\(([^)]+)\\)/i", $field, $matches)) return false;
                if(strtoupper($matches[1])=="PRIMARY") $fieldInfo->dataType = "PRIMARY";
                elseif(strtoupper($matches[1])=="UNIQUE") $fieldInfo->dataType = "UNIQUE";
                elseif(strtoupper($matches[1])=="FULLTEXT") $fieldInfo->dataType = "FULLTEXT";
                else $fieldInfo->dataType = "INDEX";
                if($matches[2])
                    $fieldName = "key_".str_replace("'","",str_replace('"','',str_replace("`","",$matches[2]))); 
                elseif($fieldInfo->dataType=="PRIMARY")
                    $fieldName = "PRIMARY";
                else 
                    $fieldName = $fieldInfo->dataType . (++$idxCounter);
                $f = str_replace("'","",str_replace('"','',str_replace("`","",$matches[3])));
                $fieldInfo->properties = array_map("trim", explode(",", str_replace("'","",str_replace('"','',str_replace("`","",$matches[3])))));
            } else {
                $info = explode(" ", $field);
                $fieldName = str_replace("'","",str_replace('"','',str_replace("`","",$info[0]))); 
                $fieldInfo->dataType = strtoupper($info[1]);
                if(stripos($field,"PRIMARY")) {
                    $fields['PRIMARY'] = new DatabaseField();
                    $fields['PRIMARY']->dataType='PRIMARY';
                    $fields['PRIMARY']->properties=array($fieldName);
                }
                if($info[1]!='TEXT' && $info[1]!='BLOB' && stripos($field,"NULL") && !stripos($field,"NOT NULL")) $fieldInfo->nullable = true;
                if(preg_match("/DEFAULT '?([^ ,']+)'?/i", $field, $matches)) $fieldInfo->default = $matches[1];
                if(stripos($field,"CURRENT_TIMESTAMP")) $fieldInfo->properties .= "CURRENT_TIMESTAMP ";
                if(stripos($field,"AUTO_INCREMENT")) $fieldInfo->properties .= "AUTO_INCREMENT ";
            }
            $fields[$fieldName] = $fieldInfo;
        }
        $retval[$tableName] = $fields;
    }
    return $retval;
}

/**
 * Loads a database definition contained in a .SQL file
 * and parse it into a usable array format
 * @param string filename
 * @return array (table name => array (field name => DatabaseField)) describing schema
 */
function loadSchemaFromFile($filename, $prefix='') {
    if(!is_readable($filename)) return false;
    $schema = str_replace("{PREFIX}",$prefix,file_get_contents($filename));
    return loadSchemaFromString($schema);
}
    
/**
 * Loads the database definition of the current database
 * into a usable array format
 * @param MyDB database object
 * @return array (table name => array (field name => DatabaseField)) describing schema
 */
function loadSchemaFromDatabase($db, $prefix='') {
    if(!$db->isOpen()) return false;
    $schema = '';
    foreach($db->execute("SHOW TABLES".($prefix?" LIKE '$prefix%'":""))->iterator() as $row) {
        foreach($row as $table) $schema .= $db->execute("SHOW CREATE TABLE $table")->fetchField(1) . ";";
    }
    return loadSchemaFromString($schema);
}

/**
 * Compare two schemas and produce SQL that will update the older to match the newer.
 * This is non-destructive (fields in the old not present in the new will not be removed),
 * but changing data types runs the risk of data loss.
 * @param array old database schema, as loaded from a loadSchema function
 * @param array new database schema, ditto
 * @return array sequence of CREATE TABLE and ALTER TABLE statements
 */
function diffSchemas($old, $new) {
    $sql = array();
    foreach($new as $table => $fields) {
        $lines = array();
        if(!isset($old[$table])) {
            $keys = array();
            $charset = 'utf8';
            foreach($fields as $name => $field) {
                if($name == 'DEFAULT CHARSET') {
                    $charset = $field;
                } else if($field->dataType == "PRIMARY") {
                    $keys[] = "PRIMARY KEY (" . implode(", ", $field->properties) . ")";
                } elseif($field->dataType == "UNIQUE" || $field->dataType == "FULLTEXT" || $field->dataType == "INDEX") {
                    if($field->dataType!='INDEX') $idx = $field->dataType . " INDEX"; else $idx = "INDEX";
                    $keys[] = "$idx `{$name}` (" . implode(", ", $field->properties) . ")";
                } else {
                    $line = "$name {$field->dataType}";
                    if($field->default!==false) $line .= " DEFAULT '{$field->default}'";
                    if($field->nullable) $line .= " NULL"; else $line .= " NOT NULL";
                    $lines[] = trim("$line {$field->properties}");
                }
            }
            $sql[] = "CREATE TABLE $table (" . implode(", ", array_merge($lines, $keys)) . ") DEFAULT CHARSET=$charset";
        } else {
            foreach($fields as $name => $field) {
                if($name == 'DEFAULT CHARSET') {
                    if(!isset($old[$table]['DEFAULT CHARSET']) || $old[$table]['DEFAULT CHARSET'] != $field) 
                        $lines[] = "CONVERT TO CHARACTER SET $field, DEFAULT CHARACTER SET=$field";
                } else if(!isset($old[$table][$name]) && $field->dataType!='UNIQUE' && $field->dataType!='FULLTEXT' && $field->dataType!='INDEX') {
                    if($field->dataType=='PRIMARY')
                        $lines[] = "ADD PRIMARY KEY (" . implode(", ", $field->properties) . ")";
                    else {
                        $line = "ADD COLUMN `$name` {$field->dataType}";
                        if($field->default!==false) $line .= " DEFAULT '{$field->default}'";
                        if($field->nullable) $line .= " NULL"; else $line .= " NOT NULL";
                        $lines[] = trim("$line {$field->properties}");
                    }
                } else {
                    // Okay, here's the hard part. Field already exists, so need to check for changes in properties
                    if($field->dataType == 'PRIMARY') {
                        if(indexMatch($field->properties, $old[$table]['PRIMARY']->properties)) continue;
                        $lines[] = "DROP PRIMARY KEY";
                        $lines[] = "ADD PRIMARY KEY (" . implode(", ", $field->properties) . ")";
                    } elseif($field->dataType == 'UNIQUE' || $field->dataType == 'FULLTEXT' || $field->dataType == 'INDEX') {
                        // Since the "names" of these indices are irrelevant, I search the old schema for matches
                        $ok = false;
                        foreach($old[$table] as $oldname => $oldfield) {
                            if($oldfield->dataType!=$field->dataType) continue;
                            if(indexMatch($oldfield->properties, $field->properties)) {
                                $ok = true;
                                break;
                            }
                        }
                        if(!$ok) $lines[] = "ADD {$field->dataType} (" . implode(", ", $field->properties) . ")";
                    } else {
                        $oldfield = $old[$table][$name];
                        if(dataTypeMatch($oldfield->dataType, $field->dataType) && 
                           ($oldfield->default == $field->default || $field->default===false) && 
                           $oldfield->nullable == $field->nullable && $oldfield->properties == $field->properties)
                            continue; // we match!
                        if($oldfield->nullable===false) $olddef='false'; else $olddef=$oldfield->nullable;
                        if($field->nullable===false) $def='false'; else $def=$field->nullable;
                        $line = "CHANGE COLUMN $name $name {$field->dataType}";
                        if($field->default!==false) $line .= " DEFAULT '{$field->default}'";
                        if($field->nullable) $line .= " NULL"; else $line .= " NOT NULL";
                        $lines[] = trim("$line {$field->properties}");
                    }
                }
            }
            if(count($lines)) $sql[] = "ALTER TABLE $table " . implode(", ", $lines);
        }
    }
    return $sql;
}

?>
