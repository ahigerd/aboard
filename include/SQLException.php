<?php
/**
 * SQLException is raised by database objects when a query fails.
 */
class SQLException extends Exception
{
    /**
     * Constructor of SQLException.
     *
     * This Exception always uses 64 as it's exception code. This should correspond to 
     * {@link SQL_ERROR} defined in constants.php, but that's just for convenience. There is
     * no codependancy involved.
     *
     * @param string message
     */
    function __construct( $msg )
    {
        parent::__construct( $msg, 64 );
    }

    function __toString( ) { return $this->getMessage( ); }

}
?>
