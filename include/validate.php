<?php

/**
 * Confirms a username contains no illegal characters
 * @param string attempted username
 * @return bool true if acceptable
 */
function isUsername($value) {
    return preg_match("/^[-A-Za-z0-9. '_]{1,32}$/", $value);
}

/**
 * Confirms a password is acceptable
 * @param string attempted password
 * @return bool true if acceptable
 */
function isPassword($value) {
    return strlen($value)>=4;
}

/**
 * Confirms an e-mail address follows a general format
 * @param string attempted e-mail address
 * @param bool allow empty addresses
 * @return bool true if acceptable
 */
function isEmail($value, $allowEmpty = false) {
    if(strlen($value)>255) return false;
    if($allowEmpty && strlen($value)==0) return true;
    return preg_match("/^[-_.A-Za-z0-9]+@[-_.A-Za-z0-9]+$/", $value);
}

/**
 * Verify that an avatar is a valid link and has valid dimensions
 * @param string URL to avatar
 * @return bool true if acceptable
 * @post global $avatarInfo array set
 */
function isAvatar($url) {
    global $config, $avatarInfo;
    if($url=='') return true;
    if(strlen($url)>255) return false;
    if(vercmp(PHP_VERSION, '5.1.0')==-1)
        $size = strlen(file_get_contents($url));
    else
        $size = strlen(file_get_contents($url, false, stream_context_create(), 0, ($config['setup']['avatarMaxSize']+1)*1024));
    if(!$size || $size>$config['setup']['avatarMaxSize']*1024) return false;
    $avatarInfo = @getimagesize($url);
    return $avatarInfo && $avatarInfo[0]<=$config['setup']['avatarMaxWidth'] && $avatarInfo[1]<=$config['setup']['avatarMaxHeight'];
}

/**
 * Verify that a signature is an acceptable size
 * @param string signature to validate
 * @return bool true if acceptable
 */
function isSignature($sig) {
    global $config;
    if(substr_count($sig,"\n")>$config['setup']['sigMaxLines']) return false;
    return strlen($sig)<=$config['setup']['sigMaxLength'];
}

/**
 * Verifies that a string will fit in the database
 * @param string the string
 * @return bool length is <= 255
 */
function isChar255($string) {
    return strlen($string)<=255;
}

/**
 * Verifies that the current user is allowed to set a title
 * @param string title to be set
 * @return bool true if acceptable
 */
function canSetTitle($title) {
    global $config;
    if(strlen($title)>255) return false;
    return $config['user']['titlePerm'] || $config['user']['rank']>=4;
}

/**
 * Confirms that a particular theme ID exists
 * @param int theme ID
 * @return bool true if exists
 */
function themeExists($themeID) {
    global $db;
    if($themeID=='') return true;
    if(!is_numeric($themeID)) return false;
    return $db->execute("SELECT COUNT(*) FROM {$prefix}_themes WHERE themeID='$themeID'")->fetchField();
}

/**
 * Confirms a user's group-aware permission
 * @param array "names" array (see forum.php et al)
 * @param int minimum rank
 * @return bool true if permitted
 */
function compareRank($names, $targetRank, $actingTarget = false) {
    global $config;
    if($actingTarget === false) $actingTarget = $targetRank;
    if($config['user']['realRank'] >= 4) return true; // admins can do everything
    if($names['rank'] == -1 || $names['actingRank'] == -1) return false; // banned users can do nothing
    if($names['actingRank'] == '') return $config['user']['rank'] >= $targetRank;
    return $names['actingRank'] >= $actingTarget;
}
 
/**
 * Confirm a user's login cookie
 * @pre database must be available
 * @pre userID and hash cookies must be set
 * @return int logged in user's ID, or false
 */
function validateLogin() {
    global $db, $config, $prefix;
    static $defaultUser = array('username'=>'Guest', 'viewSig'=>true, 'viewAvatar'=>true, 'viewSmilies'=>true, 'quickReply'=>true, 'rank'=>0, 'realRank'=>0, 'pms'=>0);
    if(!empty($_COOKIE['userID']) && !empty($_COOKIE['hash']) && ctype_digit($_COOKIE['userID'])) {
        $userID = $_COOKIE['userID'];
        $query = "SELECT u.*, COUNT(DISTINCT p.msgID) AS pms
                  FROM {$prefix}_users u LEFT JOIN {$prefix}_privmsg p ON u.userID=p.toID AND p.isRead='0' AND p.toDeleted='0'
                  WHERE u.userID='$userID' GROUP BY u.userID";
        $config['user'] = $db->execute($query)->fetchAssoc();
        $config['user']['realRank'] = $config['user']['rank'];
        if($_COOKIE['hash']!=$config['user']['password'] || !$config['user']['regConfirmed'] || !$config['user']['adminConfirmed'] ||
           !$config['user']['coppaConfirmed']) {
            $config['user'] = $defaultUser;
            clearCookie("userID");
            clearCookie("hash");
            return 0; 
        } else {
            storeCookie("hash", $config['user']['password']);
            $db->execute("UPDATE {$prefix}_users SET lastView=NOW() WHERE userID='$userID'");
            if(basename($_SERVER['SCRIPT_FILENAME'])!='rules.php' && !$config['user']['rulesConfirmed']) {
                header("Location: rules.php");
                exit;
            }
            return $userID;
        }
    } else {
        $config['user'] = $defaultUser;
        return 0; 
    }
}

