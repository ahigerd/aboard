<?php
include_once('include/config.php');

$screen = newPage($errors['local']['faq']);
$body = new Template("faq.body.html");

$rows = '';
$toc = '';
$i = 0;
foreach($db->execute("SELECT * FROM {$prefix}_faq ORDER BY position ASC")->iterator() as $row) {
    $i++;
    $line = new Template("faq.line.html");
    $line->assign("NUMBER", $i);
    $line->assign("FAQID", $row['faqID']);
    $line->assign("QUESTION", formatText($row['question']));
    $line->assign("ANSWER", formatText($row['answer']));
    $rows .= $line->html();
    $line = new Template("faq.toc.html");
    $line->assign("NUMBER", $i);
    $line->assign("FAQID", $row['faqID']);
    $line->assign("QUESTION", formatText($row['question']));
    $toc .= $line->html();
}

$body->assign("TOC", $toc);
$body->assign("ROWS", $rows);
$screen->assign("BODY", $body->html());
echo $screen->html();
?>
