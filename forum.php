<?php
if(!isset($_REQUEST['forumID']) || !is_numeric($_REQUEST['forumID'])) {
    header("Location: index.php");
    exit;
}

include_once('include/config.php');

$forumID = $_REQUEST['forumID'];
if(is_numeric($_REQUEST['page'])) {
    $start = ($_REQUEST['page']-1) * $config['setup']['threadsPerPage']; 
    $page = $_REQUEST['page'];
} else {
    $start = 0;
    $page = 1;
}

if($forumID==-1) {
    $query = "SELECT \"{$errors['local']['recent']}\" AS name, COUNT(DISTINCT t.threadID) AS tc, 0 AS minimumRead, 5 AS minimumThread, NULL AS actingRank ";
} else {
    $query = "SELECT f.name, COUNT(DISTINCT t.threadID) AS tc, f.minimumRead, f.minimumThread, IFNULL(MAX(ga.actingRank), a.actingRank) AS actingRank, ";
    $query .= "IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank, g.name AS groupName, ";
    $query .= "f.groupID, th.title AS themeName, th.cssFile, th.imagePath, th.templatePath "; 
}
$query .= "FROM {$prefix}_forums f LEFT JOIN {$prefix}_threads t ON t.forumID=f.forumID ".
          "LEFT JOIN {$prefix}_access a ON (f.forumID=a.forumID AND a.userID='$userID') ";
if($forumID != -1) {
    $query .= "LEFT JOIN {$prefix}_groups g ON f.groupID=g.groupID ".
              "LEFT JOIN {$prefix}_themes th ON g.themeID=th.themeID ";
    $query .= "LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$config['user']['userID']}' ";
    $query .= "LEFT JOIN {$prefix}_group_members gm ON gm.userID='{$config['user']['userID']}' ";
    $query .= "LEFT JOIN {$prefix}_group_access ga ON gm.groupID=ga.groupID AND ga.forumID=f.forumID ";
    $query .= "WHERE f.forumID='$forumID' GROUP BY f.forumID";
} else {
    $query .= "LEFT JOIN {$prefix}_posts l ON l.threadID=t.threadID LEFT JOIN {$prefix}_views v ON (v.threadID=t.threadID AND v.userID='$userID') ";
    $query .= "WHERE (l.timestamp>CASE WHEN v.lastView IS NULL THEN '{$config['user']['lastReset']}' ELSE v.lastView END) ";
    $query .= "AND ((a.actingRank IS NULL AND f.minimumRead<='{$config['user']['rank']}') OR (a.actingRank>=0)) AND f.groupID IS NULL";
}
$name = $db->execute($query)->fetchAssoc();

if($forumID != -1) setGroupByForumID($forumID, $name);

if(!compareRank($name, $name['minimumRead'], 1)) {
    header("Location: index.php");
    exit;
}

$name['tc'] += $db->execute("SELECT COUNT(*) FROM {$prefix}_threads WHERE forumID!='$forumID' AND isSticky=2")->fetchField();

$screen = newPage($name['name']);
$table = new Template("forum.body.html");
$table->assign("FORUMNAME", $name['name']);
$table->assign("FORUMID", $forumID);
$table->condition("PAGER", $name['tc']>$config['setup']['threadsPerPage']);
$table->assign("PAGERTOP", makePager('forum.php?forumID='.$forumID, $name['tc'], $config['setup']['threadsPerPage'], $page, true)); 
$table->assign("PAGERBOTTOM", makePager('forum.php?forumID='.$forumID, $name['tc'], $config['setup']['threadsPerPage'], $page, true)); 

if($forumID == -1) {
    $fromClause = "{$prefix}_forums f INNER JOIN {$prefix}_threads t ON t.forumID=f.forumID ";
    $subwhere = "sp.timestamp > '{$config['user']['lastReset']}'";
    $accessJoin = "LEFT JOIN {$prefix}_access a ON (f.forumID=a.forumID AND a.userID='$userID')";
    $whereClause = "(l.timestamp>CASE WHEN v.lastView IS NULL THEN '{$config['user']['lastReset']}' ELSE v.lastView END) ";
    $whereClause .= "AND ((a.actingRank IS NULL AND f.minimumRead<='{$config['user']['rank']}') OR (a.actingRank>=0)) AND f.groupID IS NULL";
} else {
    $fromClause = "{$prefix}_threads t ";
    $subwhere = "(st.forumID='$forumID' OR st.isSticky=2)";
    $accessJoin = '';
    $whereClause = "(t.forumID='$forumID' OR t.isSticky=2)";
}
$query = "SELECT t.*, u2.userID AS posterID, COUNT(DISTINCT p.postID) AS posts, l.timestamp AS lastPostAt, l.creatorID AS lastPosterID, 
                 CASE WHEN u1.userID IS NULL THEN p.creatorName ELSE u1.username END AS starter, 
                 CASE WHEN u2.userID IS NULL THEN p.creatorName ELSE u2.username END AS poster, 
                 CASE WHEN u3.userID IS NULL THEN l.creatorName ELSE u3.username END AS lastPoster,
                 CASE WHEN v.lastView IS NULL THEN '{$config['user']['lastReset']}' ELSE v.lastView END < l.timestamp AS newPosts
          FROM $fromClause
          INNER JOIN {$prefix}_posts p ON t.threadID=p.threadID 
          LEFT JOIN {$prefix}_users u1 ON t.creatorID=u1.userID 
          LEFT JOIN {$prefix}_users u2 ON p.creatorID=u2.userID 
          LEFT JOIN {$prefix}_views v ON (v.threadID=t.threadID AND v.userID='$userID')
          LEFT JOIN (SELECT sq.threadID, sq.title, sq.stamp AS timestamp, sl.postID, sl.creatorName, sl.creatorID
                     FROM (SELECT sp.threadID, MAX(timestamp) AS stamp, st.isSticky, st.title
                           FROM {$prefix}_posts sp
                           INNER JOIN {$prefix}_threads st ON sp.threadID=st.threadID 
                           WHERE $subwhere
                           GROUP BY sp.threadID) AS sq
                     LEFT JOIN {$prefix}_posts sl ON sl.threadID=sq.threadID AND sl.timestamp=sq.stamp) AS l ON t.threadID = l.threadID
          LEFT JOIN {$prefix}_users u3 ON l.creatorID=u3.userID 
          $accessJoin
          WHERE $whereClause 
          GROUP BY t.threadID
          ORDER BY isSticky DESC, lastPostAt DESC
          LIMIT $start, {$config['setup']['threadsPerPage']}";

$body = '';
foreach($db->execute($query)->iterator() as $row) {
    $line = new Template("forum.line.html");
    $status = "icon";
    $line->condition("TOLAST", $row['newPosts']);
    if($row['newPosts']) {
        $status .= '-new';
        $statusAlt = 'New Posts';
    } else {
        $status .= '-no-new';
        $statusAlt = 'No New Posts';
    }
    if($row['isSticky'] == 1) {
        $status .= '-sticky';
        $line->assign("TYPE", "Sticky");
    } elseif($row['isSticky'] == 2) {
        $status .= '-announce';
        $line->assign("TYPE", "Announcement");
    } elseif($row['isSticky'] == 3) {
        $status .= '-announce';
        $line->assign("TYPE", "Global Announcement");
    } else {
        $line->assign("TYPE", "");
        if($row['posts']>$config['setup']['hotThreadSize']) {
            $status .= '-hot';
            $statusAlt = "Hot! ".$statusAlt;
        }
    }
    if($row['isLocked']) {
        $status .= '-locked';
        $statusAlt .= " - Locked";
    }
    $line->assign("STATUS", imageLink($status.".gif", $statusAlt));
    $line->assign("THREADID", $row['threadID']);
    $line->assign("TITLE", $row['title']);
    $line->assign("PAGER", makePager("thread.php?threadID=$row[threadID]", $row['posts'], $config['setup']['postsPerPage'], 0, false));
    $line->assign("POSTCOUNT", $row['posts']);
    if($row['creatorID'])
        $line->assign("STARTER", "<a href='user.php?id=$row[creatorID]'>$row[starter]</a>");
    else
        $line->assign("STARTER", $row['starter']);
    $line->assign("LASTPOST", formatTime($row['lastPostAt']));
    if($row['lastPosterID'])
        $line->assign("LASTPOSTER", "<a href='user.php?id=$row[lastPosterID]'>$row[lastPoster]</a>");
    else
        $line->assign("LASTPOSTER", $row['lastPoster']);
    $body .= $line->html();
}

$table->condition("THREAD", compareRank($name, $name['minimumThread'], 1));

$table->assign("ROWS", $body);
$screen->assign("BODY", $table->html());

echo $screen->html();
?>
