<?php
include_once('include/config.php');

if(!empty($_GET['group']) && ctype_digit($_GET['group'])) 
    $groupID = $_GET['group'];
else
    $groupID = null;
setGroup($groupID);

$screen = newPage();
$table = new Template("index.body.html");

// Query for most recent post for each forum. This would be impractical (impossible?) to put into the main query,
// and querying for each forum in the loop would be inefficient, so we just query it all up front.
$query = "SELECT t.forumID, t.threadID, t.title, p.timestamp, ";
$query .= "CASE WHEN u.username IS NULL THEN p.creatorName ELSE u.username END AS poster, u.userID ";
$query .= "FROM {$prefix}_threads t INNER JOIN {$prefix}_posts p ON t.threadID=p.threadID ";
$query .= "LEFT JOIN {$prefix}_users u ON p.creatorID=u.userID ";
$subquery = "SELECT MAX(timestamp) AS stamp, t.forumID ";
$subquery .= "FROM {$prefix}_posts p INNER JOIN {$prefix}_threads t ON p.threadID=t.threadID ";
$subquery .= "GROUP BY t.forumID";
$query .= "WHERE ";
foreach($db->execute($subquery)->iterator() as $stamp)
    $query .= "(p.timestamp='$stamp[stamp]' AND t.forumID='$stamp[forumID]') OR ";
$query .= "1=0 "; // We want this query to return 0 rows if there aren't any posts
$query .= "GROUP BY t.threadID";
$lastPost = array();
foreach($db->execute($query)->iterator() as $row)
    $lastPost[$row['forumID']] = $row;

// Query for moderator list, for similar reasons
// We assemble it as an array of arrays, indexed by forumID and userID
$mods = array();
$query = "SELECT a.forumID, u.userID, u.username FROM {$prefix}_users u ";
$query .= "INNER JOIN {$prefix}_access a ON u.userID=a.userID ";
$query .= "WHERE actingRank>=2";
foreach($db->execute($query)->iterator() as $mod) {
    if(!isset($mods[$mod['forumID']]))
        $mods[$mod['forumID']] = array($mod['userID']=>$mod['username']);
    else
        $mods[$mod['forumID']][$mod['userID']] = $mod['username'];
}
$query = "SELECT a.forumID, u.userID, u.username FROM {$prefix}_users u ";
$query .= "INNER JOIN {$prefix}_group_members g ON u.userID=g.userID ";
$query .= "INNER JOIN {$prefix}_group_access a ON g.groupID=a.groupID ";
$query .= "INNER JOIN {$prefix}_forums f ON a.forumID=f.forumID ";
$query .= "WHERE a.actingRank>=2";
foreach($db->execute($query)->iterator() as $mod) {
    if(!isset($mods[$mod['forumID']]))
        $mods[$mod['forumID']] = array($mod['userID']=>$mod['username']);
    else
        $mods[$mod['forumID']][$mod['userID']] = $mod['username'];
}
/*
// Turns out this isn't necessary; rank 2 doesn't imply anything and rank 3+ shouldn't show up
if($groupID) {
    $query = "SELECT f.forumID, u.userID, u.username FROM {$prefix}_users u ";
    $query .= "INNER JOIN {$prefix}_group_members g ON u.userID=g.userID ";
    $query .= "INNER JOIN {$prefix}_forums f ON g.groupID=f.groupID ";
    $query .= "WHERE g.rank>=3 AND g.groupID=$groupID";
    foreach($db->execute($query)->iterator() as $mod) {
        if(!isset($mods[$mod['forumID']]))
            $mods[$mod['forumID']] = array($mod['userID']=>$mod['username']);
        else
            $mods[$mod['forumID']][$mod['userID']] = $mod['username'];
    }
}
*/

// Query category list
$cats = array();
$query = "SELECT forumID, name FROM {$prefix}_forums WHERE isCategory='1'";
foreach($db->execute($query)->iterator() as $row)
    $cats[$row['forumID']] = $row['name'];

if($userID) {
    $query = "SELECT DISTINCT t.forumID FROM {$prefix}_threads t INNER JOIN {$prefix}_posts p ON t.threadID=p.threadID ";
    $query .= "LEFT JOIN {$prefix}_views v ON (v.userID='$userID' AND v.threadID=p.threadID) ";// AND v.lastView<p.timestamp) ";
    $query .= "WHERE (v.threadID IS NULL AND p.timestamp > '{$config['user']['lastReset']}') OR v.lastView<p.timestamp";
    $unread = $db->execute($query)->getResultArray('forumID');
} else {
    $unread = array();
}

// Query forum list
$query = "SELECT f.forumID, f.parentID, f.name, f.description, COUNT(DISTINCT t.threadID) AS threads, f.isCategory, ";
$query .= "COUNT(DISTINCT p.postID) AS posts, f.groupID, f.minimumRead, ";
if($userID)
    $query .= "IFNULL(MAX(ga.actingRank), a.actingRank) AS actingRank, IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank ";
else
    $query .= "NULL AS actingRank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END AS rank ";
$query .= "FROM {$prefix}_forums f LEFT JOIN {$prefix}_forums pf ON f.parentID=pf.forumID ";
if($userID) $query .= "LEFT JOIN {$prefix}_access a ON (f.forumID=a.forumID AND a.userID='$userID') ";
$query .= "LEFT JOIN {$prefix}_threads t ON f.forumID=t.forumID ";
$query .= "LEFT JOIN {$prefix}_posts p ON t.threadID=p.threadID ";
$query .= "LEFT JOIN {$prefix}_groups g ON f.groupID=g.groupID ";
$query .= "LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='$userID' ";
$query .= "LEFT JOIN {$prefix}_group_members gm ON gm.userID='$userID' ";
$query .= "LEFT JOIN {$prefix}_group_access ga ON gm.groupID=ga.groupID AND ga.forumID=f.forumID ";
$query .= "WHERE (f.parentID IS NULL OR f.parentID IN (NULL";
foreach($db->execute("SELECT forumID FROM {$prefix}_forums WHERE isCategory='1'")->iterator() as $row)
    $query .= ','.$row['forumID'];
$query .= ")) ";
if($groupID) {
    $query .= "AND f.groupID={$groupID} ";
    if($userID && $config['user']['rank'] < 3) $query .= "AND (gm.userID={$userID} OR ga.actingRank IS NOT NULL) "; 
} else {
    $query .= "AND f.groupID IS NULL ";
}
if(!$userID) {
    $query .= "AND f.minimumRead<=0 ";
}
$query .= "AND f.isCategory=0 ";
$query .= "GROUP BY f.forumID ORDER BY CASE WHEN f.parentID IS NULL THEN f.sortOrder ELSE pf.sortOrder END, ";
$query .= "CASE WHEN f.parentID IS NULL THEN -1 ELSE f.sortOrder END";
$rows = '';
foreach($db->execute($query)->iterator() as $row) {
    if(!compareRank($row, $row['minimumRead'], 1)) continue;
    if(isset($cats[$row['parentID']])) {
        $line = new Template("index.category.html");
        $line->assign("NAME",$cats[$row['parentID']]);
        $rows .= $line->html();
        unset($cats[$row['parentID']]);
    }
    $line = new Template("index.line.html");
    $hasNew = in_array($row['forumID'], $unread);
    $line->condition("NEW", $hasNew); 
    if(!$hasNew)
        $line->assign("NEW",imageLink("icon-no-new.gif", "No New Posts"));
    else 
        $line->assign("NEW",imageLink("icon-new.gif", "New Posts"));
    $line->assign("ID",$row['forumID']);
    $line->assign("DESC",$row['description']);
    $line->assign("THREADS",$row['threads']);
    $line->assign("POSTS",$row['posts']);
    if(!isset($lastPost[$row['forumID']])) {
        $line->condition("NOLAST", true);
        $line->assign("LAST", ''); 
    } else {
        $line->condition("NOLAST", false);
        $last = new Template("index.last.html");
        $last->assign("THREADID", $lastPost[$row['forumID']]['threadID']);
        $last->assign("TITLE", $lastPost[$row['forumID']]['title']);
        $last->assign("POSTER", $lastPost[$row['forumID']]['poster']);
        $last->assign("TIMESTAMP", formatTime($lastPost[$row['forumID']]['timestamp']));
        $last->condition("NOTGUEST",$lastPost[$row['forumID']]['userID']);
        $last->assign("POSTERID", $lastPost[$row['forumID']]['userID']);
        $line->assign("LAST", $last->html());
    }
    if(isset($mods[$row['forumID']])) {
        $modList = '';
        foreach($mods[$row['forumID']] as $userID => $username)
            $modList .= "<a href='user.php?userID=$userID'>$username</a>,";
        $line->condition("MODS", true);
        $line->assign("MODS", substr($modList,0,-1));
    } else {
        $line->condition("MODS", false);
        $line->assign("MODS", '');
    }
    $line->assign("NAME",$row['name']);
    $rows .= $line->html();
}
if($rows == '') {
    if($groupID) {
        header("Location: index.php");
        exit;
    } else {
        $rows = $errors['general']['no_forums'];
    }
}
$table->assign("ROWS",$rows);

$table->assign("USERS", $db->execute("SELECT COUNT(*) FROM {$prefix}_users WHERE adminConfirmed='1' AND regConfirmed='1' AND coppaConfirmed='1'")->fetchField());
$table->assign("POSTS", $db->execute("SELECT COUNT(*) FROM {$prefix}_posts")->fetchField());
$table->assign("THREADS", $db->execute("SELECT COUNT(*) FROM {$prefix}_threads")->fetchField());
$newest = $db->execute("SELECT userID, username FROM {$prefix}_users WHERE adminConfirmed='1' AND regConfirmed='1' AND coppaConfirmed='1' ORDER BY userID DESC LIMIT 1")->fetchAssoc();
$table->assign("NEWESTID", $newest['userID']);
$table->assign("NEWEST", $newest['username']);

$userlist = '';
foreach($db->execute("SELECT userID, username, rank FROM {$prefix}_users ".
    "WHERE ".($config['user']['rank']>1?"invisible='0' AND ":'')."lastView>NOW() - INTERVAL 5 MINUTE ORDER BY username")->iterator() as $u)
    $userlist .= "<a href='user.php?id={$u['userID']}' class='rank".($u['rank']+1)."'>{$u['username']}</a>, ";
$table->assign("USERLIST", substr($userlist, 0, -2));

$screen->assign("BODY",$table->html());

echo $screen->html();
?>
