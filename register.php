<?php
include_once('include/config.php');
include_once('include/mail.php');

if($config['setup']['regDisable']) {
    header("Location: index.php");
    exit;
}

$error = array();
if(isset($_POST['submit'])) {
    if(empty($_POST['uname']) || empty($_POST['passwd']) || empty($_POST['passwd_again']) || (empty($_POST['email']) && !$config['setup']['regNoEmail']))
        $error[] = $errors['general']['form_incomplete'];
    if(!isUsername($_POST['uname']))
        $error[] = $errors['register']['username_invalid'];
    
    try {
        $query = "SELECT COUNT(*) FROM {$prefix}_users WHERE username='".addslashes($_POST['uname'])."'";
        $name_check = $db->execute($query)->fetchField();
    } catch (SQLException $e) {
        die($e->getMessage());
    }

    if($name_check != 0) 
        $error[] = $errors['register']['username_exists'];
    if(!isPassword($_POST['passwd']))
        $error[] = $errors['register']['password_too_short'];
    elseif($_POST['passwd'] != $_POST['passwd_again'])
        $error[] = $errors['register']['password_mismatch'];
    if(!isEmail($_POST['email'], $config['setup']['regNoEmail'])) 
        $error[] = $errors['register']['email_invalid'];
    if($config['setup']['regReqVisual']) {
        $str = verifyString($_POST['key']);
        if (strtolower($_POST['visual'])!=strtolower($str)) $error[] = $errors['register']['visual_mismatch'];
    }

    if(empty($error)) {
        try {
            $key = verifyString($_REQUEST['key'],6);
            $insert = "INSERT INTO {$prefix}_users SET username = '".addslashes($_POST['uname'])."', password = '".md5($_POST['passwd'])."', ";
            $insert .= "registerIP = '$_SERVER[REMOTE_ADDR]', registerdate = NOW(), email = '".$_POST['email']."', ";
            $insert .= "adminConfirmed='".(1-$config['setup']['regReqAdmin'])."', denyShowEmail='".(empty($_REQUEST['coppa'])?'1':'0')."', ";
            $insert .= "coppaConfirmed='".(empty($_REQUEST['coppa'])?'0':'1')."', hideEmail='".(empty($_REQUEST['coppa'])?'1':'0')."', ";
            $insert .= "regConfirmed='".(1-$config['setup']['regReqEmail'])."', activationKey='$key'";
            $db->execute($insert);
            if($config['setup']['regReqEmail'] || empty($_REQUEST['coppa'])) {
                $newUserID = $db->execute("SELECT userID FROM {$prefix}_users WHERE username='".addslashes($_POST['uname'])."'")->fetchField();
                $mail = new Template("register.email.txt");
                $mail->condition("CONFIRM", $config['setup']['regReqEmail']);
                $mail->condition("COPPA", empty($_REQUEST['coppa']));
                $mail->assign("SERVER", $_SERVER['SERVER_NAME']);
                $mail->assign("ADMINMAIL", $config['setup']['adminEmail']);
                $mail->assign("ID", $newUserID);
                $mail->assign("KEY", rawurlencode($key));
                sendMail($newUserID, $config['setup']['forumName'].' '.
                    $errors['register']['confirm_subject'], $mail->html());
            }
            $db->execute("DELETE FROM {$prefix}_keys WHERE code='$_REQUEST[key]'");
            $screen = newPage("Registration");
            $body = new Template("register.confirm.html");
            $body->assign("EMAIL", $_REQUEST['email']);
            $body->assign("ADMINMAIL", $config['setup']['adminEmail']);
            $body->condition("EMAIL", !$config['setup']['regNoEmail']);
            $body->condition("CONFIRM", ($config['setup']['regReqEmail'] || !empty($_REQUEST['coppa'])) && !empty($_REQUEST['email']));
            $body->condition("COPPAMAIL", empty($_REQUEST['coppa']) && !empty($_REQUEST['email']));
            $body->condition("COPPANOMAIL", empty($_REQUEST['coppa']) && empty($_REQUEST['email']));
            $body->condition("ADMIN", $config['setup']['regReqAdmin'] || empty($_REQUEST['coppa']));
            $body->condition("ACTIVE", !$config['setup']['regReqEmail'] && !empty($_REQUEST['coppa']) && !$config['setup']['reqRegAdmin']);
            $screen->assign("BODY", $body->html());
            echo $screen->html();
            exit;
        } catch (SQLException $e) {
            die($e->getMessage());
        }
    }
}

$screen = newPage($errors['local']['registration']);
$table = new Template("register.body.html");
$table->condition("ERROR", !empty($error));
$table->assign("ERROR", implode("<br/>",$error));
$table->assign("USERNAME", empty($_POST['uname'])?'':$_POST['uname']);
$table->assign("EMAIL", empty($_POST['email'])?'':$_POST['email']);
$table->condition("EMAIL", !$config['setup']['regNoEmail']);
$table->condition("VISUAL", $config['setup']['regReqVisual']);
$table->assign("KEY", empty($_REQUEST['key'])?rand():$_REQUEST['key']);
$table->assign("STAMP", md5(time()));
$table->assign("COPPA", empty($_REQUEST['coppa'])?'':'checked="true"');
$screen->assign("BODY", $table->html());
echo $screen->html();

?>
