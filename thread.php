<?php
if(!isset($_REQUEST['threadID']) || !is_numeric($_REQUEST['threadID'])) {
    header("Location: index.php");
    exit;
}

include_once('include/config.php');

$threadID = $_REQUEST['threadID'];
setGroupByThreadID($threadID);

$newest=-1;
if(isset($_REQUEST['page']) && is_numeric($_REQUEST['page'])) {
    if($_REQUEST['page']==-1) {
        $timestamp = $db->execute("SELECT MAX(timestamp) FROM {$prefix}_posts WHERE threadID='$threadID'")->fetchField(); 
        $newest = $db->execute("SELECT MIN(postID) FROM {$prefix}_posts WHERE threadID='$threadID' AND timestamp='$timestamp'")->fetchField();
        $page = $db->execute("SELECT COUNT(*) FROM {$prefix}_posts WHERE threadID='$threadID' AND postID<='$newest'")->fetchField();
        $page = ceil($page / $config['setup']['postsPerPage']);
        $start = ($page-1) * $config['setup']['postsPerPage']; 
    } else {
        $start = ($_REQUEST['page']-1) * $config['setup']['postsPerPage']; 
        $page = $_REQUEST['page'];
    }
} else {
    if(isset($_REQUEST['postID']) && is_numeric($_REQUEST['postID'])) {
        $stats = $db->execute("SELECT COUNT(*)+1 AS ct FROM {$prefix}_posts ".
            "WHERE threadID='$threadID' AND postID<'$_REQUEST[postID]'")->fetchAssoc();
        $page = ceil($stats['ct'] / $config['setup']['postsPerPage']);
        $start = ($page-1) * $config['setup']['postsPerPage']; 
    } else {
        $start = 0;
        $page = 1;
    }
}

$names = "SELECT t.forumID, f.name, t.title, COUNT(DISTINCT p.postID) AS pc, minimumRead, minimumPost, minimumMod, ".
        "isSticky, isLocked, lastView, poll, g.name AS groupName, ";
if($userID)
    $names .= "IFNULL(MAX(ga.actingRank), a.actingRank) AS actingRank, IFNULL(m.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank ";
else
    $names .= "NULL AS actingRank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END AS rank ";
$names .= "FROM {$prefix}_forums f INNER JOIN {$prefix}_threads t ON t.forumID=f.forumID ";
$names .= "LEFT JOIN {$prefix}_groups g ON f.groupID=g.groupID ".
          "LEFT JOIN {$prefix}_themes th ON g.themeID=th.themeID ";
$names .= "LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$config['user']['userID']}' ";
$names .= "LEFT JOIN {$prefix}_group_members gm ON gm.userID='{$config['user']['userID']}' ";
$names .= "LEFT JOIN {$prefix}_group_access ga ON gm.groupID=ga.groupID AND ga.forumID=f.forumID ";
$names .= "LEFT JOIN {$prefix}_access a ON (a.forumID=t.forumID AND a.userID='$userID') ".
          "LEFT JOIN {$prefix}_views v ON (v.userID='$userID' AND v.threadID=t.threadID) ".
          "INNER JOIN {$prefix}_posts p ON p.threadID=t.threadID WHERE t.threadID='$threadID' GROUP BY t.threadID";
$names = $db->execute($names)->fetchAssoc();

if(!$names || !compareRank($names, $names['minimumRead'], 1)) {
    header("Location: index.php");
    exit;
}

$canPost = compareRank($names, $names['minimumPost'], 1); 
if($canPost && $db->execute("SELECT COUNT(*) FROM {$prefix}_kicks WHERE threadID='$threadID' AND userID='$userID'")->fetchField()) $canPost = false;
$canEdit = (!$config['user']['denyEdit'] && $canPost);
$canMod = compareRank($names, $names['minimumMod'], 2) && $canPost;
$canAnnounce = $canPost && $config['user']['rank']>=3;
if($names['isLocked'] && !$canMod) { $canPost = false; $canEdit = false; }

$screen = newPage($names['title']);
$table = new Template("thread.body.html");
$table->assign("FORUMNAME", $names['name']);
$table->assign("FORUMID", $names['forumID']);
$table->assign("THREADNAME", $names['title']);
$table->assign("THREADID", $threadID);
$table->condition("PAGER", $names['pc']>$config['setup']['postsPerPage']);
$table->assign("PAGERTOP", makePager('thread.php?threadID='.$threadID, $names['pc'], $config['setup']['postsPerPage'], $page, true)); 
$table->assign("PAGERBOTTOM", makePager('thread.php?threadID='.$threadID, $names['pc'], $config['setup']['postsPerPage'], $page, true)); 

if($names['poll'] != '') {
    $poll = new Template("poll.html");
    $total = $db->execute("SELECT SUM(hits) FROM {$prefix}_polls WHERE threadID='$threadID'")->fetchField();
    $poll->assign("TOTAL", $total);
    if(!$total) $total=1;
    $poll->assign("TITLE", $names['poll']);
    $poll->assign("THREADID", $threadID);
    $options = '';
    $pollmode = "result";
    if($userID && !$db->execute("SELECT COUNT(*) FROM {$prefix}_votes WHERE threadID='$threadID' AND userID='$userID'")->fetchField())
        $pollmode = "option";
    foreach($db->execute("SELECT optionID, caption, hits FROM {$prefix}_polls WHERE threadID=$threadID ORDER BY optionID ASC")->iterator() as $option) {
        $line = new Template("poll.$pollmode.html");
        $line->assign("OPTIONID", $option['optionID']);
        $line->assign("CAPTION", htmlspecialchars($option['caption']));
        if($pollmode == 'result') {
            $pct = floor($option['hits']/$total*100);
            $line->assign("PERCENT", $pct); 
        }
        $options .= $line->html();
    }
    $poll->assign("OPTIONS", $options);
    $poll->condition("VOTE", $pollmode == 'option');
    $table->assign("POLL", $poll->html());
} else {
    $table->assign("POLL", '');
}

$ranks = array(-1=>array(), 0=>array(), 1=>array(), 2=>array(), 3=>array(), 4=>array());
foreach($db->execute("SELECT rank, minPosts, image, title FROM {$prefix}_ranks")->iterator() as $row) {
    $ranks[$row['rank']][$row['minPosts']] = array("image"=>$row['image'], "title"=>$row['title']);
}

$query = "SELECT p.postID, u.userID, u.title, CASE WHEN u.userID IS NULL THEN p.creatorName ELSE u.username END AS name, p.timestamp, ";
$query .= "p.body, p.disableTags, p.disableSig, p.disableSmilies, u.signature, u.avatarURL, u.avatarHeight, u.avatarWidth, u.location, ";
$query .= "CASE WHEN u.rank IS NULL THEN 0 ELSE u.rank END AS rank, u.customRank, COUNT(DISTINCT p2.postID) AS pc, p.creatorIP, ";
$query .= "p.creatorID, (v.threadID IS NULL OR v.lastView<p.timestamp) AS isNew, COUNT(DISTINCT k.threadID) AS isKicked ";
$query .= "FROM {$prefix}_posts p LEFT JOIN {$prefix}_threads t ON p.threadID=t.threadID ";
$query .= "LEFT JOIN {$prefix}_users u ON p.creatorID=u.userID LEFT JOIN {$prefix}_views v ON (v.userID='$userID' AND v.threadID=t.threadID) ";
$query .= "LEFT JOIN {$prefix}_posts p2 ON u.userID=p2.creatorID LEFT JOIN {$prefix}_kicks k ON p.threadID=k.threadID AND p.creatorID=k.userID ";
$query .= "WHERE p.threadID='$threadID' GROUP BY p.postID LIMIT $start, ".$config['setup']['postsPerPage'];

$body = '';
$even = true;
foreach($db->execute($query)->iterator() as $row) {
    $even = !$even;
    $line = new Template("thread.line.html");
    $line->condition("EVEN", $even);
    $line->assign("POSTID", $row['postID']);
    $line->assign("THREADID", $threadID);
    $line->assign("NEWPOST", imageLink("icon-minipost".($row['isNew']?'-new':'').".gif", ($row['isNew']?"New Post":"Link")));
    $line->assign("WIDTH", $config['setup']['avatarMaxWidth']);
    $line->condition("MOD", $canMod);
    $line->condition("KICK", $canMod && $row['creatorID'] && ($row['creatorID'] != $userID) && !$row['isKicked']);
    $line->condition("QUOTE", $canPost);
    $line->condition("PM", !$config['user']['denyPM'] && $row['creatorID']);
    $line->condition("EDIT", ($canEdit && $userID==$row['creatorID']) || $canMod);
    $line->assign("POSTERID", $row['creatorID']);
    $line->assign("IP", $row['creatorIP']);
    $line->condition("NEWEST", $row['postID']==$newest); 
    $line->condition("SIG", $row['signature']&&!$row['disableSig']&&$config['user']['viewSig']);
    $line->condition("LOCATION", $row['location']);
    $line->assign("LOCATION", $row['location']);
    if($row['userID']) $line->assign("POSTER","<a href='user.php?id=$row[userID]'>$row[name]</a>");
    else $line->assign("POSTER", $row['name']);
    $line->assign("TIMESTAMP", formatTime($row['timestamp']));
    $formatMode = ($row['disableTags'] ? FORMAT_NO_TAGS : 0) | ($row['disableSmilies'] ? FORMAT_NO_SMILIES : 0); 
    $line->assign("BODY", formatText($row['body'], $row['creatorID'], FORMAT_POST | $formatMode));
    $line->assign("SIG", formatText($row['signature'], $row['creatorID'], FORMAT_SIG));
    $line->condition("AVATAR", $row['avatarURL']&&$config['user']['viewAvatar']);
    if($row['avatarURL']) $line->assign("AVATAR", "<img src='$row[avatarURL]' height='$row[avatarHeight]' width='$row[avatarWidth]' />");
    else $line->assign("AVATAR", "");
    if($row['title']=='' || $row['customRank']=='') {
        if(!isset($ranks[$row['rank']][$row['pc']])) {
            $t = 0;
            foreach($ranks[$row['rank']] as $pc => $rank) {
                if($row['pc']<$pc) break;
                $t = $pc;
            }
            $ranks[$row['rank']][$row['pc']] = $ranks[$row['rank']][$t];
        }
    }
    $line->assign("TITLE", $row['title']==''?$ranks[$row['rank']][$row['pc']]['title']:$row['title']);
    $line->assign("RANK", imageLink($row['customRank']==''?$ranks[$row['rank']][$row['pc']]['image']:$row['customRank']), $errors['rank'][$row['rank']]);
    $body .= $line->html();
}

$table->assign("ROWS", $body);
$table->condition("REPLY", $canPost);
$table->condition("MOD", $canMod);
$table->condition("LOCK", $canMod && !$names['isLocked']);
$table->condition("UNLOCK", $canMod && $names['isLocked']);
$table->condition("UNSTICKY", ($names['isSticky']!=0 && $canMod) && !($names['isSticky']==2 && !$canAnnounce));
$table->condition("STICKY", ($names['isSticky']!=1 && $canMod) && !($names['isSticky']==2 && !$canAnnounce));
$table->condition("ANNOUNCE", !$config['setup']['groupID'] && $names['isSticky']!=2 && $canAnnounce);
$screen->assign("BODY", $table->html());
echo $screen->html();

if($userID) $db->execute("REPLACE INTO {$prefix}_views SET userID='$userID', threadID='$threadID', lastView=NOW()");

?>
