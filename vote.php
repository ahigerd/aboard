<?php
if(!isset($_REQUEST['threadID']) || !is_numeric($_REQUEST['threadID'])) {
    header("Location: index.php");
    exit;
}

include_once('include/config.php');

if(!$userID) {
    header("Location: index.php");
    exit;
}

if(isset($_REQUEST['optionID']) && !$db->execute("SELECT COUNT(*) FROM {$prefix}_votes WHERE threadID='$_REQUEST[threadID]' AND userID='$userID'")->fetchField()) {
    $db->execute("INSERT INTO {$prefix}_votes SET threadID='$_REQUEST[threadID]', userID='$userID'");
    $db->execute("UPDATE {$prefix}_polls SET hits=hits+1 WHERE optionID='$_REQUEST[optionID]'");
}

header("Location: thread.php?threadID=$_REQUEST[threadID]&page=-1#newest");

?>
