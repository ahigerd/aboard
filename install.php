<?php
include_once("include/mysql_db.php");
include_once("include/dbUpgrader.php");
include_once("include/template.php");
include_once("include/configCore.php");
include_once("include/validate.php");

$step = (empty($_POST['step'])?0:$_POST['step']);
$error = '';

if(isset($config['temp']['addAdmin'])) {
    $db = new MyDB($config['db']['host'], $config['db']['username'], $config['db']['password'], $config['db']['database']);
    $db->open();
    if(!$db->execute("SELECT COUNT(*) FROM {$config['aboard']['prefix']}_users")->fetchField())
        $step = 2;
    else {
        $step = 3;
    }
}

if(isset($_POST['submit']) && $step==0) {
    if(!preg_match("/^[A-Za-z0-9_]*$/", $_POST['prefix'])) {
        $error = "<tr><td colspan='2' align='center'><i>Prefix may only contain letters, numbers, and underscores.</i></td></tr>";
    } else try {
        $db = new MyDB($_POST['host'], $_POST['username'], $_POST['password'], $_POST['database']);
        $db->open();
        $config['temp']['host'] = $_POST['host'];
        $config['temp']['username'] = $_POST['username'];
        $config['temp']['password'] = $_POST['password'];
        $config['temp']['database'] = $_POST['database'];
        $config['temp']['prefix'] = $_POST['prefix'];
        $step = 1;
    } catch(Exception $e) {
        $error = "<tr><td colspan='2' align='center'><i>Unable to connect to database!</i></td></tr>";
    }
    if($step==1) try {
        writeini("aboard2.ini", $config);
    } catch(Exception $e) {
        $step = -1;
    } else {
        $config['db']['host'] = $_POST['host'];
        $config['db']['database'] = $_POST['database'];
        $config['db']['username'] = $_POST['username'];
        $config['aboard']['prefix'] = $_POST['prefix'];
    }
} elseif($step==1) {
    try {
        $db = new MyDB($config['temp']['host'], $config['temp']['username'], $config['temp']['password'], $config['temp']['database']);
        $db->open();

        $old = loadSchemaFromDatabase($db, $config['temp']['prefix']);
        $new = loadSchemaFromFile("schema.sql", $config['temp']['prefix']);
        $upgrade = diffSchemas($old, $new);
        foreach($upgrade as $statement) {
            // Uncomment for debugging
            // echo $statement;
            $db->execute($statement);
        }
        if($db->execute("SELECT COUNT(*) FROM {$config['temp']['prefix']}_themes")->fetchField() == 0) {
            $db->execute("INSERT INTO {$config['temp']['prefix']}_themes (title, cssFile, imagePath, templatePath)
                          VALUES ('ABoard Default', 'default.css', 'images', 'templates')");
        }
        
        if($config['aboard']['version'] == 'none') {
            $_POST['username'] = '';
            $_POST['email'] = '';
            $step = 2;
        } else {
            $step = 3;
        }
        
        $config['aboard']['prefix'] = $config['temp']['prefix'];
        $config['aboard']['version'] = VERSION;
        $config['db'] = $config['temp'];
        $config['db']['mysql'] = $db->execute("SHOW VARIABLES LIKE 'version'")->fetchField(1);
        unset($config['db']['prefix']);
        unset($config['temp']);
        $config['temp']['addAdmin'] = 1;
        writeini("aboard2.ini", $config);
    } catch(Exception $e) {
        // Uncomment for debugging
        // echo $db->error();
        $step = -2;
    }
} elseif(isset($_POST['submit']) && $step==2) {
    if(!isUsername($_POST['username'])) {
        $error = "<tr><td colspan='2' align='center'><i>Usernames must be between 1 and 32 characters long and contain ";
        $error .= "only letters, numbers, spaces, hyphens, apostrophes, periods, and underscores.</i></td></tr>";
    }
    if(!isEmail($_POST['email']))
        $error .= "<tr><td colspan='2' align='center'><i>Please enter a valid e-mail address.</i></td></tr>";
    if($_POST['password']!=$_POST['verify'])
        $error .= "<tr><td colspan='2' align='center'><i>Passwords do not match.</i></td></tr>";
    if(!$error) try {
        $db->execute("INSERT INTO {$config['aboard']['prefix']}_users (username, password, rank, email, regConfirmed, registerDate, coppaConfirmed) VALUES ".
                '("'.$_POST['username'].'", "'.md5($_POST['password']).'", 4, "'.$_POST['email'].'", 1, NOW(), 1)');
        $db->execute("INSERT INTO {$config['aboard']['prefix']}_config (adminEmail) VALUES ('{$_POST['email']}')");
        $step = 3;
    } catch (Exception $e) {
        $step = -3;
    }
} elseif($step==3) {
    unset($config['temp']);
    writeini("aboard2.ini", $config);
}

switch($step) {
case 0:
    $screen = new Template("setup.db.html");
    $screen->assign("HOST", $config['db']['host']);
    $screen->assign("DATABASE", $config['db']['database']);
    $screen->assign("USERNAME", $config['db']['username']);
    $screen->assign("PREFIX", $config['aboard']['prefix']);
    $screen->assign("ERROR", $error);
    break;
case 1:
    $screen = new Template("setup.confirm.html");
    $screen->assign("HOST", $config['temp']['host']);
    $screen->assign("PREFIX", $config['temp']['prefix']);
    $screen->assign("DATABASE", $config['temp']['database']);
    $screen->assign("OLDVERSION", $config['aboard']['version']);
    if($config['aboard']['version']=='none')
        $screen->assign("BUTTON", "Install");
    else
        $screen->assign("BUTTON", "Upgrade");
    break;
case 2:
    $screen = new Template("setup.user.html");
    $screen->assign("ERROR", $error);
    $screen->assign("USERNAME", $_POST['username']);
    $screen->assign("EMAIL", $_POST['email']);
    break;
case 3:
    $screen = new Template("setup.complete.html");
    break;
case -1:
    echo "<html><head><title>ABoard ".VERSION." Installation</title></head><body>";
    echo "ABoard was unable to write to its configuration file. ";
    echo "This is probably caused by incorrect file or directory permissions.<br/><br/>";
    echo "Installation cannot continue. Please contact your server administrator.</body></html>";
    die();
case -2:
    echo "<html><head><title>ABoard ".VERSION." Installation</title></head><body>";
    echo "ABoard was unable to modify the database structure. ";
    echo "The database user must have CREATE TABLE and ALTER TABLE privileges on the specified database. ";
    echo "These permissions may be removed after installation or upgrading is complete.<br/><br/>";
    echo "Installation cannot continue. Please contact your server administrator.</body></html>";
    die();
case -3:
    echo "<html><head><title>ABoard ".VERSION." Installation</title></head><body>";
    echo "ABoard was unable to create an administrative user. ";
    echo "This probably indicates a problem with database permissions.<br/><br/>";
    echo "Installation has been completed, but an administrative user could not be created. ";
    echo "Please contact your system administrator and then return to the ";
    echo "<a href='install.php'>installer</a> to try again.</body></html>";
    die();
}

$screen->assign("VERSION", VERSION);
echo $screen->html();
?>
