<?php
if(!isset($_REQUEST['threadID']) && isset($_REQUEST['forumID']) && is_numeric($_REQUEST['forumID']))
    $_REQUEST['threadID']=0; 
if(!isset($_REQUEST['threadID']) || !is_numeric($_REQUEST['threadID'])) {
    header("Location: index.php");
    exit;
}
if(!$_REQUEST['threadID'] && (empty($_REQUEST['forumID']) || !is_numeric($_REQUEST['forumID']))) {
    header("Location: index.php");
    exit;
}

include_once('include/config.php');

$threadID = $_REQUEST['threadID'];

if($threadID==0) {
    setGroupByForumID($_REQUEST['forumID']);
    $query = "SELECT COUNT(DISTINCT threadID) FROM {$prefix}_threads WHERE forumID='{$_REQUEST['forumID']}' AND creatorID='{$userID}'";
    $tc = $db->execute($query)->fetchField();
    $query = "SELECT f.forumID, f.name, '' AS title, minimumThread AS minRank, threadsPerUser, ";
    if($userID)
        $query .= "IFNULL(MAX(ga.actingRank), a.actingRank) AS actingRank, IFNULL(gm.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank ";
    else
        $query .= "NULL AS actingRank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END AS rank ";
    $query .= "FROM {$prefix}_forums f ";
    $query .= "LEFT JOIN {$prefix}_groups g ON f.groupID=g.groupID ";
    $query .= "LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$config['user']['userID']}' ";
    $query .= "LEFT JOIN {$prefix}_group_members gm ON gm.userID='{$config['user']['userID']}' ";
    $query .= "LEFT JOIN {$prefix}_group_access ga ON gm.groupID=ga.groupID AND ga.forumID=f.forumID ";
    $query .= "LEFT JOIN {$prefix}_access a ON (a.forumID=f.forumID AND a.userID='$userID') WHERE f.forumID='$_REQUEST[forumID]'";
} else {
    setGroupByThreadID($threadID);
    $query = "SELECT t.forumID, f.name, title, minimumPost AS minRank, ";
    if($userID)
        $query .= "CASE WHEN COUNT(k.userID) > 0 THEN -1 ELSE IFNULL(MAX(ga.actingRank), a.actingRank) END AS actingRank, IFNULL(gm.rank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END) AS rank ";
    else
        $query .= "NULL AS actingRank, CASE WHEN g.isPrivate=1 THEN -1 ELSE 0 END AS rank ";
    $query .= "FROM {$prefix}_forums f INNER JOIN {$prefix}_threads t ON t.forumID=f.forumID ";
    $query .= "LEFT JOIN {$prefix}_kicks k ON (t.threadID=k.threadID AND k.userID='$userID') ";
    $query .= "LEFT JOIN {$prefix}_groups g ON f.groupID=g.groupID ";
    $query .= "LEFT JOIN {$prefix}_group_members m ON g.groupID=m.groupID AND m.userID='{$config['user']['userID']}' ";
    $query .= "LEFT JOIN {$prefix}_group_members gm ON gm.userID='{$config['user']['userID']}' ";
    $query .= "LEFT JOIN {$prefix}_group_access ga ON gm.groupID=ga.groupID AND ga.forumID=f.forumID ";
    $query .= "LEFT JOIN {$prefix}_access a ON (a.forumID=f.forumID AND a.userID='$userID') ";
    $query .= "WHERE t.threadID='$threadID' GROUP BY t.threadID"; 
}

$names = $db->execute($query)->fetchAssoc();
if(!$names) {
    header("Location: index.php");
    exit;
}

if($names['groupID'] && !$names['groupStatus']) {
    // you're not in the group
    if($names['actingRank'] < 1 && $userID && $config['user']['rank'] >= 3)
        $names['actingRank'] = $config['user']['rank']; // if you're a super mod, come on in
    else
        $names['actingRank'] = 0;
}

$deny = false;
if($threadID==0 && $names['threadsPerUser']!=-1 && $names['threadsPerUser']<=$tc) {
    $deny = true;
} else if($threadID == 0 && !compareRank($names, $names['minimumThread'], 1)) {
    $deny = true;
} else if(!compareRank($names, $names['minimumPost'], 1)) {
    $deny = true;
}

if($deny) {
    $screen = newPage();
    $page = new Template("post.denied.html");
    $screen->assign("BODY", $page->html());
    echo $screen->html();
    exit;
}

$error = "";
if(isset($_POST['post']) || isset($_POST['preview'])) {
    if($threadID==0 && empty($_REQUEST['subject'])) $error .= $errors['post']['no_subject'] . '<br/>';
    if(empty($_REQUEST['body'])) $error .= $errors['post']['no_body'].'<br/>';
    $poll = 0;
    if(!empty($_REQUEST['polloptions'])) {
        foreach($_REQUEST['polloptions'] as $option) {
            if($option!='') $poll++;
        }
    }
    if($poll && empty($_REQUEST['polltitle'])) $error .= $errors['post']['no_poll_title'].'<br/>';
    if($poll < 2 && !empty($_REQUEST['polltitle'])) $error .= $errors['post']['no_poll_options'].'<br/>';
}

if(!$error && isset($_POST['post'])) {
    if($threadID==0) {
        $db->insert($prefix."_threads", array("creatorID"=>$userID, "creatorName"=>$config['user']['username'], "title"=>$_REQUEST['subject'],
                    "forumID"=>$_REQUEST['forumID'], "poll"=>empty($_REQUEST['polltitle'])?null:$_REQUEST['polltitle']));
        $threadID = $db->execute("SELECT LAST_INSERT_ID()")->fetchField();
        if(!$threadID) {
            throw new Exception("Unable to create thread");
        }
        foreach($_REQUEST['polloptions'] as $option) {
            if($option=='') continue;
            $db->insert($prefix."_polls", array("threadID"=>$threadID, "caption"=>$option));
        }
    }
    $db->insert($prefix."_posts", array("creatorID"=>$userID, "creatorName"=>$config['user']['username'], "body"=>$_REQUEST['body'],
                "disableSig"=>(isset($_REQUEST['disableSig'])?'1':'0'), "disableSmilies"=>(isset($_REQUEST['disableSmilies'])?'1':'0'),
                "disableTags"=>(isset($_REQUEST['disableTags'])?'1':'0'), "threadID"=>$threadID, "timestamp"=>$_NOW, "creatorIP"=>$_SERVER['REMOTE_ADDR']));
    if($userID) {
        if(!$db->execute("SELECT COUNT(*) FROM {$prefix}_posts p INNER JOIN {$prefix}_views v ON p.threadID=v.threadID ".
           "WHERE userID='$userID' AND p.timestamp>v.lastView")->fetchField())
            $db->execute("REPLACE INTO {$prefix}_views SET userID='$userID', threadID='$threadID', lastView=NOW()");
    }
    header("Location: thread.php?threadID=$threadID&page=-1#newest");
    exit;
}

$page = new Template("post.html");
if($threadID==0)
    $screen = newPage($errors['local']['new_thread']);
else
    $screen = newPage($errors['local']['posting_in'].' '.$names['title']);

if(!isset($_REQUEST['subject'])) $_REQUEST['subject']=$names['title'];
$page->assign("FORUMNAME", $names['name']);
$page->assign("FORUMID", $names['forumID']);
$page->condition("THREAD", !$threadID);
$page->assign("THREADNAME", htmlspecialchars($names['title'], ENT_QUOTES));
$page->assign("THREADID", $threadID);
$page->assign("SUBJECT", htmlspecialchars($_REQUEST['subject'], ENT_QUOTES));

if(!$threadID) {
    $page->assign("POLLTITLE", empty($_REQUEST['polltitle'])?'':htmlspecialchars($_REQUEST['polltitle'], ENT_QUOTES));
    if(empty($_REQUEST['pollcount']) || $_REQUEST['pollcount'] < 2) $_REQUEST['pollcount'] = 2;
    if(!empty($_REQUEST['addpoll'])) $_REQUEST['pollcount']++;
    $options = "<input type='hidden' name='pollcount' id='pollCount' value='".$_REQUEST['pollcount']."' />";
    $line = new Template("post.poll.html");
    $line->assign("VALUE", '');
    $options .= "<div id='pollTemplate' style='display:none'>".$line->html()."</div>";
    if(empty($_REQUEST['polloptions'])) $_REQUEST['polloptions']=array();
    if(isset($_REQUEST['polloptions']['{ID}'])) unset($_REQUEST['polloptions']['{ID}']);
    $optid = 0;
    if(isset($_REQUEST['deleteoption'])) {
        $_REQUEST['pollcount']--;
        foreach($_REQUEST['deleteoption'] as $key => $value)
            if(isset($_REQUEST['polloptions'][$key])) unset($_REQUEST['polloptions'][$key]);
    }
    foreach($_REQUEST['polloptions'] as $option) {
        $line = new Template("post.poll.html");
        $line->assign("ID", $optid);
        $line->assign("VALUE", $option);
        $options .= $line->html();
        $optid++;
    }
    for(;$optid < $_REQUEST['pollcount']; $optid++) {
        $line = new Template("post.poll.html");
        $line->assign("ID", $optid);
        $line->assign("VALUE", '');
        $options .= $line->html();
    }
    $page->assign("POLLOPTIONS", $options);
}

if(isset($_REQUEST['postID']) && is_numeric($_REQUEST['postID'])) {
    $replyTo = $db->execute("SELECT creatorName, body FROM {$prefix}_posts WHERE postID='$_REQUEST[postID]'")->fetchAssoc();
    $body = "[quote=$replyTo[creatorName]]$replyTo[body][/quote]";
} elseif(isset($_REQUEST['body'])) {
    $body = $_REQUEST['body'];
} else {
    $body = '';
}

$page->condition("PREVIEW", isset($_POST['preview']));
$disableTags = !empty($_REQUEST['disableTags']);
$disableSmilies = !empty($_REQUEST['disableSmilies']);
if(isset($_POST['preview'])) {
    $formatMode = ($disableTags ? FORMAT_NO_TAGS : 0) | ($disableSmilies ? FORMAT_NO_SMILIES : 0);
    $page->assign('PREVIEW', formatText($body, $userID, FORMAT_PREVIEW | $formatMode));
}
if(isset($_POST['preview']) && $spellError) $error .= $errors['post']['spelling']; 
$page->condition("ERROR", $error);
$page->assign("ERROR", $error);

if(!isset($_POST['post']) && !isset($_POST['preview']) && $config['user']['disableSmilies'])
    $disableSmilies = true;

$page->condition("DISABLESIG", !empty($_REQUEST['disableSig']));
$page->condition("DISABLETAGS", $disableTags);
$page->condition("DISABLESMILIES", $disableSmilies);

$page->assign("BODY", htmlspecialchars($body, ENT_QUOTES));

$screen->assign("BODY", $page->html());
echo $screen->html();
?>
