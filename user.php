<?php
if(isset($_REQUEST['id']) && !is_numeric($_REQUEST['id'])) {
    header("Location: index.php");
    exit;
}

include_once('include/config.php');

if(isset($_REQUEST['id'])) $id = $_REQUEST['id']; else $id = $userID;

if($config['user']['rank'] < 2)
    $where = "AND regConfirmed='1' AND adminConfirmed='1' AND coppaConfirmed='1'";
else
    $where = '';
$user = $db->execute("SELECT * FROM {$prefix}_users WHERE userID='$id' $where")->fetchAssoc();
if(!$user) {
    header("Location: index.php");
    exit;
}

if(isset($_POST['profile'])) {
    $settings = array('viewSig'=>'0','viewAvatar'=>'0','viewSmilies'=>'0','quickReply'=>'0','themeID'=>'',
        'invisible'=>'0','hideEmail'=>'0','popupPM'=>'0','emailPM'=>'0','email'=>'', 'location'=>'', 'bio'=>'',
        'timeZone'=>'','useDst'=>'0','avatarURL'=>'','signature'=>'','disableSmilies'=>'0','title'=>'');
    $validate = array('email'=>'isEmail','password'=>'isPassword', 'avatarURL'=>'isAvatar', 'title'=>'isChar255', 'location'=>'isChar255',
        'timeZone'=>'is_numeric', 'signature'=>'isSignature', 'title'=>'canSetTitle', 'themeID'=>'themeExists');
    if(!empty($_POST['password'])) $settings['password']='';
    foreach($_POST as $field => $value) {
        if(!isset($settings[$field])) continue;
        if($settings[$field]=='0' && $value!='1') $error .= $errors['profile']['hack_attempt']."<br/>";
        else if(isset($validate[$field]) && !call_user_func($validate[$field], $value)) $error .= $errors['profile'][$field]."<br/>";
        else if($field=='password' && (empty($_POST['verify']) || $value!=$_POST['verify'])) $error .= $errors['profile']['password']."<br/";
        $settings[$field] = ($value==''?null:$value);
        if($field=='avatarURL' && !empty($avatarInfo)) {
            $settings['avatarWidth'] = $avatarInfo[0];
            $settings['avatarHeight'] = $avatarInfo[1];
            $_POST['avatarWidth'] = $avatarInfo[0];
            $_POST['avatarHeight'] = $avatarInfo[1];
        } 
    }
    $db->setSloppyNullStrings();
    if(empty($settings['password']))
        unset($settings['password']);
    else
        $settings['password'] = md5($settings['password']);
    if(!$error) {
        $db->update($prefix."_users", $settings, 'userID', $userID);
        header("Location: user.php");
        exit;
    }
}

$user['pc'] = $db->execute("SELECT COUNT(*) FROM {$prefix}_posts WHERE creatorID='$id'")->fetchField();

$screen = newPage($errors['local']['user_cp']);

$isEditMode = ($user['userID'] == $userID) && empty($_REQUEST['view']);
if($isEditMode) {
    $body = new Template("user.edit.html");
    $body->condition("CHANGE", $config['user']['titlePerm']);
} else {
    $body = new Template("user.profile.html");
}

$body->assign("USERID", $user['userID']);
$body->assign("USERNAME", $user['username']);
$body->assign("REGDATE", formatTime($user['registerDate']));
$body->assign("POSTCOUNT", $user['pc']);
$body->assign("TIMEZONE", ($user['timeZone']==''?$config['setup']['timeZone']:$user['timeZone']));
$rank = $db->execute("SELECT image, title FROM {$prefix}_ranks WHERE rank='{$user['rank']}' AND minPosts<'{$user['pc']}' ".
    "ORDER BY minPosts DESC LIMIT 1")->fetchAssoc();
$body->assign("RANK", $errors['rank'][$user['rank']]);
$body->assign("RANKIMG", imageLink($user['customRank']?$user['customRank']:$rank['image']));
$body->condition("SETTITLE", $user['titlePerm'] || $config['user']['rank']>=4);
if($user['titlePerm'] || $config['user']['rank']>=4 || $user['title']!='')
    $body->assign("TITLE", $user['title']);
else
    $body->assign("TITLE", $rank['title']);
$body->assign("EMAIL", (!$userID || ($user['hideEmail'] && $user['userID'] != $userID)) ? $errors['local']['hidden'] : $user['email']);
$body->condition("HIDEMAIL", $user['hideEmail'] && !$user['denyShowEmail']);
$body->condition("CANSHOWMAIL", !$user['denyShowEmail']);
$body->assign("LOCATION", $user['location']);
$body->condition("AVATAR", $user['avatarURL'] && (!isset($_POST['avatarURL']) || !empty($avatarInfo)));
$body->assign("AVATAR", $user['avatarURL']);
$body->assign("HEIGHT", $user['avatarHeight']);
$body->assign("WIDTH", $user['avatarWidth']);
$body->condition("SETAVATAR", !$user['denyAvatar']);
$body->condition("SIGNATURE", $user['signature']);
if($isEditMode)
    $body->assign("SIGNATURE", htmlspecialchars($user['signature']));
else
    $body->assign("SIGNATURE", formatText($user['signature'], $user['userID'], FORMAT_SIG));
$body->condition("SETSIGNATURE", !$user['denySig']);
$body->assign("BIO", $user['bio']);
$body->condition("VIEWSIG", $user['viewSig']);
$body->condition("VIEWAVATAR", $user['viewAvatar']);
$body->condition("VIEWSMILIES", $user['viewSmilies']);
$body->condition("QUICKREPLY", $user['quickReply']);
$body->condition("INVIS", $user['invisible']);
$body->condition("POPUPPM", $user['popupPM']);
$body->condition("EMAILPM", $user['emailPM']);
$body->condition("USEDST", $user['useDst']);
$body->condition("DISSMILIES", $user['disableSmilies']);
$body->condition("ERROR", $error);
$body->assign("ERROR", $error);

$screen->assign("BODY", $body->html());
echo $screen->html();

?>
