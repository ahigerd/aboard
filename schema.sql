CREATE TABLE {PREFIX}_users (
    userID INTEGER AUTO_INCREMENT,
    username VARCHAR(32) NOT NULL,
    password VARCHAR(32) NOT NULL,
    
    postCount INTEGER DEFAULT 0,
    rank TINYINT DEFAULT 1,
    customRank VARCHAR(255) NULL,
    title VARCHAR(255) NULL,
    titlePerm TINYINT DEFAULT 0,
    lastView DATETIME NULL,
    lastReset DATETIME NULL,
    
    avatarURL VARCHAR(255) NULL,
    avatarHeight SMALLINT DEFAULT 0,
    avatarWidth SMALLINT DEFAULT 0,
    signature TEXT NULL,
    email VARCHAR(255) NULL,
    location VARCHAR(255) NULL,
    bio TEXT NULL,
    
    viewSig TINYINT DEFAULT 1,
    viewAvatar TINYINT DEFAULT 1,
    viewSmilies TINYINT DEFAULT 1,
    disableSmilies TINYINT DEFAULT 0,
    quickReply TINYINT DEFAULT 1,
    themeID SMALLINT NULL,
    timeZone SMALLINT NULL,
    useDst TINYINT DEFAULT 1,
    invisible TINYINT DEFAULT 0,
    hideEmail TINYINT DEFAULT 0,
    popupPM TINYINT DEFAULT 1,
    emailPM TINYINT DEFAULT 0,
    
    registerIP CHAR(16) NOT NULL,
    registerDate DATETIME NULL,
    regConfirmed TINYINT DEFAULT 0,
    adminConfirmed TINYINT DEFAULT 1,
    coppaConfirmed TINYINT DEFAULT 0,
    rulesConfirmed TINYINT DEFAULT 0,
    activationKey CHAR(6) NULL,
    
    denyPM TINYINT DEFAULT 0,
    denyThread TINYINT DEFAULT 0,
    denyAvatar TINYINT DEFAULT 0,
    denySig TINYINT DEFAULT 0,
    denyEdit TINYINT DEFAULT 0,
    denyTags TINYINT DEFAULT 0,
    denyShowEmail TINYINT DEFAULT 0,
    banMessage TEXT NULL,
    banNotes TEXT NULL,
    banExpires DATETIME NULL,
    
    PRIMARY KEY (userID),
    UNIQUE INDEX (username)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_notes (
    noteID INTEGER AUTO_INCREMENT PRIMARY KEY,
    userID INTEGER NOT NULL,
    creatorID INTEGER NOT NULL,
    body TEXT NOT NULL,
    INDEX (userID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_forums (
    forumID SMALLINT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    description TEXT NULL,
    sortOrder TINYINT DEFAULT 0,
    parentID TINYINT NULL,
    isCategory TINYINT DEFAULT 0,
    minimumRead TINYINT DEFAULT 0,
    minimumThread TINYINT DEFAULT 1,
    minimumPost TINYINT DEFAULT 1,
    minimumMod TINYINT DEFAULT 3,
    threadsPerUser TINYINT DEFAULT -1,
    groupID INTEGER NULL,
    PRIMARY KEY (forumID),
    INDEX (sortOrder)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_access (
    forumID TINYINT,
    userID INTEGER,
    actingRank TINYINT DEFAULT 2, -- -1 = can't read (only useful if guests can't read), 0 = r/o, 1 = r/w, 2 = mod
    PRIMARY KEY (forumID, userID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_threads (
    threadID INTEGER AUTO_INCREMENT,
    forumID TINYINT NOT NULL,
    creatorID INTEGER NOT NULL,
    creatorName VARCHAR(32) NULL,
    title VARCHAR(255) NOT NULL,
    poll VARCHAR(255) NULL,
    isSticky TINYINT DEFAULT 0, -- 0 = normal, 1 = sticky, 2 = announcement
    isLocked TINYINT DEFAULT 0,
    PRIMARY KEY (threadID),
    INDEX (forumID),
    INDEX (creatorID),
    INDEX (isSticky),
    FULLTEXT (title)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_views (
    threadID INTEGER,
    userID INTEGER,
    lastView DATETIME,
    PRIMARY KEY (threadID, userID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_kicks (
    threadID INTEGER,
    userID INTEGER,
    PRIMARY KEY (threadID, userID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_posts (
    postID INTEGER AUTO_INCREMENT PRIMARY KEY,
    threadID INTEGER NOT NULL,
    creatorID INTEGER NOT NULL,
    creatorName VARCHAR(32) NULL,
    creatorIP CHAR(16) NOT NULL,
    timestamp DATETIME CURRENT_DATETIME,
    body TEXT NOT NULL,
    disableSig TINYINT DEFAULT 0,
    disableTags TINYINT DEFAULT 0,
    disableSmilies TINYINT DEFAULT 0,
    INDEX (threadID),
    INDEX (creatorID),
    FULLTEXT (body)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_polls (
    optionID INTEGER AUTO_INCREMENT PRIMARY KEY,
    threadID INTEGER NOT NULL,
    caption VARCHAR(255) NOT NULL,
    hits INTEGER DEFAULT 0,
    INDEX (threadID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_votes (
    threadID INTEGER,
    userID INTEGER,
    PRIMARY KEY (threadID, userID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_boxes (
    boxID INTEGER AUTO_INCREMENT PRIMARY KEY,
    userID INTEGER NOT NULL,
    title VARCHAR(255) NOT NULL,
    INDEX (userID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_privmsg (
    msgID INTEGER AUTO_INCREMENT PRIMARY KEY,
    fromID INTEGER NOT NULL,
    toID INTEGER NOT NULL,
    fromName VARCHAR(32) NULL,
    toName VARCHAR(32) NULL,
    boxID INTEGER DEFAULT -1,
    subject VARCHAR(255) NOT NULL,
    body TEXT NOT NULL,
    disableTags TINYINT DEFAULT 0,
    disableSmilies TINYINT DEFAULT 0,
    replyID INTEGER NULL,
    timestamp DATETIME CURRENT_DATETIME,
    fromDeleted TINYINT DEFAULT 0, -- To avoid duplication in the database,
    toDeleted TINYINT DEFAULT 0,   -- flag records as deleted until both are deleted
    isRead TINYINT DEFAULT 0,
    INDEX (fromID),
    INDEX (toID),
    INDEX (boxID),
    INDEX (replyID),
    FULLTEXT (subject, body)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_ban_names (
    username VARCHAR(32) PRIMARY KEY,
    banExpires DATETIME NULL,
    INDEX (banExpires)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_ban_ip (
    address CHAR(15) PRIMARY KEY,
    banAll TINYINT DEFAULT 0,
    banMessage TEXT NULL,
    banNotes TEXT NULL,
    banExpires DATETIME NULL,
    INDEX (banExpires)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_ban_email (
    address VARCHAR(255) PRIMARY KEY,
    banMessage TEXT NULL,
    banNotes TEXT NULL,
    banExpires DATETIME NULL,
    INDEX (banExpires)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_config (
    forumName VARCHAR(255) DEFAULT 'ABoard',
    avatarMaxHeight SMALLINT DEFAULT 100,
    avatarMaxWidth SMALLINT DEFAULT 150,
    avatarMaxSize INTEGER DEFAULT 50, -- in kilobytes
    smiliesPerPost TINYINT DEFAULT 10,
    imagesPerPost TINYINT DEFAULT 3,
    autoPruneAge INTEGER NULL, -- in days
    logRegistrations TINYINT DEFAULT 1,
    logConfirmations TINYINT DEFAULT 1,
    logEdits TINYINT DEFAULT 1,
    logStatus TINYINT DEFAULT 1,
    logMod TINYINT DEFAULT 1,
    logAdmin TINYINT DEFAULT 1,
    loginLimit TINYINT DEFAULT 3,
    loginTime INTEGER DEFAULT 60, -- in seconds
    confirmAdminLogin TINYINT DEFAULT 1,
    floodLimit TINYINT DEFAULT 1,
    floodTime INTEGER DEFAULT 60, -- in seconds
    doublePostMode TINYINT DEFAULT 2, -- 0=allow, 1=warn, 2=block
    adminEmail VARCHAR(255) NULL,
    sigMaxLength INTEGER DEFAULT 300,
    sigMaxLines TINYINT DEFAULT 3,
    mailboxSize INTEGER DEFAULT 100,
    defaultTheme INTEGER NULL,
    threadsPerPage INTEGER DEFAULT 30,
    postsPerPage INTEGER DEFAULT 30,
    hotThreadSize INTEGER DEFAULT 50,
    dateFormat VARCHAR(255) DEFAULT NULL,
    regReqEmail TINYINT DEFAULT 0,
    regReqVisual TINYINT DEFAULT 0,
    regReqAdmin TINYINT DEFAULT 0,
    regNoEmail TINYINT DEFAULT 0,
    regDisable TINYINT DEFAULT 0,
    timeZone SMALLINT DEFAULT 0,
    useDst SMALLINT DEFAULT 1,
    timeFormat VARCHAR(255) DEFAULT NULL,
    smtpHost VARCHAR(255) DEFAULT 'localhost',
    smtpPort SMALLINT DEFAULT 25,
    smtpUser VARCHAR(255) NULL,
    smtpPass VARCHAR(255) NULL
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_log (
    logID INTEGER AUTO_INCREMENT PRIMARY KEY,
    timestamp DATETIME CURRENT_DATETIME,
    userID INTEGER NOT NULL,
    logType VARCHAR(255) NOT NULL,
    body TEXT NOT NULL,
    INDEX (logType),
    INDEX (userID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_replace (
    replaceID INTEGER AUTO_INCREMENT PRIMARY KEY,
    openTag VARCHAR(255) NOT NULL,
    closeTag VARCHAR(255) NULL,
    replacement VARCHAR(255) NOT NULL,
    restricted TINYINT DEFAULT 0,
    useInSig TINYINT DEFAULT 1,
    isSmiley TINYINT DEFAULT 0,
    isCensor TINYINT DEFAULT 0,
    INDEX (isSmiley),
    INDEX (isCensor)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_favorites (
    userID INTEGER NOT NULL,
    threadID INTEGER NOT NULL,
    notify TINYINT DEFAULT 0,
    PRIMARY KEY (userID, threadID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_faq (
    faqID INTEGER AUTO_INCREMENT PRIMARY KEY,
    question TEXT,
    answer TEXT,
    position INTEGER,
    INDEX (position)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_rules (
    ruleID INTEGER AUTO_INCREMENT PRIMARY KEY,
    body TEXT,
    position INTEGER,
    INDEX (position)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_themes (
    themeID INTEGER AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255),
    subdomain VARCHAR(32),
    cssFile VARCHAR(255),
    imagePath VARCHAR(255),
    templatePath VARCHAR(255),
    INDEX (title),
    INDEX (subdomain)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_keys (
    code VARCHAR(32) PRIMARY KEY,
    secret VARCHAR(32),
    created DATETIME
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_ranks (
    rankID INTEGER AUTO_INCREMENT PRIMARY KEY,
    rank TINYINT NOT NULL,
    minPosts INTEGER DEFAULT 0,
    image VARCHAR(255) NULL,
    title VARCHAR(255) NULL,
    INDEX (rank)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_groups (
    groupID INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    themeID INTEGER NULL,
    isPrivate TINYINT DEFAULT 0
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_group_members (
    groupID INTEGER NOT NULL,
    userID INTEGER NOT NULL,
    rank TINYINT DEFAULT 1,
    PRIMARY KEY (groupID, userID)
) DEFAULT CHARSET=utf8;

CREATE TABLE {PREFIX}_group_access (
    forumID TINYINT NOT NULL,
    groupID INTEGER NOT NULL,
    actingRank TINYINT DEFAULT 1,
    PRIMARY KEY (forumID, groupID)
) DEFAULT CHARSET=utf8;
