<?php
include_once("include/config.php");

if(isset($_REQUEST['view']) && (!empty($_REQUEST['subject']) || !empty($_REQUEST['body']) || !empty($_REQUEST['creator']))) {
    $screen = newPage($errors['local']['results']);
    if(!empty($_REQUEST['creator'])) $_REQUEST['creator'] = str_replace("*","%",$_REQUEST['creator']); 
    if(empty($_REQUEST['body']) && empty($_REQUEST['creator'])) $_REQUEST['view'] = 'thread';
    if(empty($_REQUEST['page'])) $_REQUEST['page'] = 1;
    if($_REQUEST['view']=='thread') {
        $threadMode = true;
        $body = new Template("search.thread.html");
        $limit = $config['setup']['threadsPerPage'];

        $query = "SELECT SQL_CALC_FOUND_ROWS t.threadID, t.title, CASE WHEN tu.username IS NULL THEN t.creatorName ELSE tu.username END AS username, ";
        $query .= "t.creatorID, MAX(p.timestamp) AS postAt, COUNT(DISTINCT p.postID) AS postCount ";
    } else {
        $threadMode = false;
        $body = new Template("search.post.html");
        $limit = $config['setup']['postsPerPage'];

        $query = "SELECT SQL_CALC_FOUND_ROWS p.threadID, p.body, CASE WHEN pu.username IS NULL THEN p.creatorName ELSE pu.username END AS username, ";
        $query .= "p.creatorID, p.timestamp AS postAt, p.postID, p.creatorIP, pu.avatarURL, pu.avatarHeight, ";
        $query .= "pu.avatarWidth, pu.location, pu.customRank ";
    }
    $from .= "FROM {$prefix}_forums f INNER JOIN {$prefix}_threads t ON t.forumID=f.forumID ";
    $from .= "INNER JOIN {$prefix}_posts p ON t.threadID=p.threadID LEFT JOIN {$prefix}_users pu ON p.creatorID=pu.userID ";
    $from .= "LEFT JOIN {$prefix}_access a ON (f.forumID=a.forumID AND a.userID='$userID') ";
    $where = array("((a.actingRank IS NULL AND f.minimumRead<='{$config['user']['rank']}') OR (a.actingRank>=0))");
    if(!empty($_REQUEST['body'])) $where[] = "MATCH(p.body) AGAINST ('".addslashes($_REQUEST['body'])."' IN BOOLEAN MODE)";
    if(!empty($_REQUEST['subject'])) $where[] .= "MATCH(t.title) AGAINST ('".addslashes($_REQUEST['subject'])."' IN BOOLEAN MODE)";
    if(!empty($_REQUEST['forumID'])) $where[] .= "f.forumID IN (".implode(",",$_REQUEST['forumID']).")";
    if(!empty($_REQUEST['creator'])) $where[] .= "(p.creatorName LIKE '$_REQUEST[creator]' OR pu.username LIKE '$_REQUEST[creator]')"; 
    $where = "WHERE ".implode(" AND ", $where);
    $query .= $from.($threadMode?" LEFT JOIN {$prefix}_users tu ON t.creatorID=tu.userID ":' ').$where; 
    if($threadMode) $query .= "GROUP BY t.threadID ";
    $query .= "ORDER BY postAt DESC LIMIT ".(($_REQUEST['page']-1)*$limit).", $limit";
    $threads = array();
    if($threadMode)
        $count = $db->execute("SELECT COUNT(DISTINCT t.threadID) $from $where")->fetchField();
    else
        $count = $db->execute("SELECT COUNT(DISTINCT p.postID) $from $where")->fetchField();
    if($count == 0) {
        $body = new Template('search.noresults.html');
        $screen->assign("BODY", $body->html());
        die($screen->html());
    }
    if($threadMode) {
        foreach($db->execute($query)->iterator() as $row) $threads['t'.$row['threadID']] = $row;
        $query = "SELECT p.threadID, CASE WHEN u.username IS NULL THEN p.creatorName ELSE u.username END AS posterName, p.creatorID AS posterID ";
        $query .= "FROM {$prefix}_posts p LEFT JOIN {$prefix}_users u ON p.creatorID=u.userID WHERE ";
        foreach($threads as $row) $query .= "(p.threadID='$row[threadID]' AND p.timestamp='$row[postAt]') OR ";
        $query = substr($query, 0, -3);
        foreach($db->execute($query)->iterator() as $row) {
            $threads['t'.$row['threadID']]['lastPoster'] = $row['posterName'];
            $threads['t'.$row['threadID']]['lastPosterID'] = $row['posterID'];
        }
    } else {
        $threads = $db->execute($query)->iterator();
    } 
    $rows = '';
    $even = true;
    foreach($threads as $row) {
        $even = !$even;
        if($threadMode) {
            $line = new Template('search.thread.line.html');
            $line->assign("TITLE", $row['title']);
            if($row['lastPosterID'])
                $line->assign("LASTPOSTER", "<a href='user.php?id=$row[lastPosterID]'>$row[lastPoster]</a>");
            else
                $line->assign("LASTPOSTER", $row['lastPoster']);
            $line->assign("POSTCOUNT", $row['postCount']);
        } else {
            $line = new Template('search.post.line.html');
            $line->condition("MOD", $config['user']['rank']>=2);
            $line->assign("POSTID", $row['postID']);
            $line->assign("BODY", formatText($row['body']));
            $line->condition("EVEN",$even); 
            $line->assign("WIDTH", $config['setup']['avatarMaxWidth']);
            $line->condition("PM", !$config['user']['denyPM'] && $row['creatorID']);
            $line->assign("IP", $row['creatorIP']);
            $line->condition("LOCATION", $row['location']);
            $line->assign("LOCATION", $row['location']);
            $line->condition("AVATAR", $row['avatarURL']&&$config['user']['viewAvatar']);
            if($row['avatarURL']) $line->assign("AVATAR", "<img src='$row[avatarURL]' height='$row[avatarHeight]' width='$row[avatarWidth]' />");
            else $line->assign("AVATAR", "");
        }
        $line->assign("THREADID", $row['threadID']);
        if($row['creatorID'])
            $line->assign("CREATOR", "<a href='user.php?id=$row[creatorID]'>$row[username]</a>");
        else
            $line->assign("CREATOR", $row['username']);
        $line->assign("POSTAT", formatTime($row['postAt']));
        $rows .= $line->html();
    }
    $body->condition("PAGER", $count>$limit);
    $params = $_REQUEST;
    unset($params['page']);
    foreach($_REQUEST as $i=>$val) if($val=='') unset($params[$i]);
    foreach($_COOKIE as $i=>$dummy) unset($params[$i]);
    $body->assign("PAGERTOP", makePager('search.php?'.http_build_query($params), min($count, $limit*10), $limit, $_REQUEST['page'])); 
    $body->assign("PAGERBOTTOM", makePager('search.php?'.http_build_query($params), min($count, $limit*10), $limit, $_REQUEST['page'])); 
    $body->assign("ROWS", $rows);
} else {
    $screen = newPage($errors['local']['search']);
    $body = new Template("search.form.html");
    $body->assign("FORUMDROP", makeForumDrop("forumID[]", "style='width:200px'"));
}

$screen->assign("BODY", $body->html());
echo $screen->html();
?>
